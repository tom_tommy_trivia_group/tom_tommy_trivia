import socket
import json

SERVER_IP = "127.0.0.1"
SERVER_PORT = 8876

#'S' - signup
#'L' - login
#'R' - get rooms
#'P' - get players in room
#'H' - get highscores
#'J' - join room
#'C' - create room
#'O' - close room
#'G' - start game
#'T' - get room state
#'E' - leave room
#'X' - logout

# Create a TCP/IP socket
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# Connecting to remote computer 80
server_address = (SERVER_IP, SERVER_PORT)

status = ""

connected = False

while status != "OK":
	choice = ""
	while  choice.upper() != "S" and choice.upper() != "L":
		choice = input("CREATE MSG:\tchoose S or s for signup and l or L to login: ")
		if choice.upper() != "S" and choice.upper() != "L":
			print("Wrong input, please try again \n")

	Username = input("\t\tchoose USERNAME: ")
	Password = input("\t\tchoose PASWOORD: ")

	data = "{\"username\" : \""+Username+"\", \"password\" : \"" + Password + "\""

	#if the request is signup type, we need another parameter
	isSignup = (choice.upper() == "S")
	if(isSignup):
		Email = input("\t\tchoose EMAIL: ")
		data += " , \"email\" : \"" + Email + "\""
		msgType = "S"
	else:
		msgType = "L"

	data += "}"
	MSG = msgType + str(len(data)).zfill(4) + data
	print("sent:\t",MSG)

	
	if not connected:
		#connect to the server
		sock.connect(server_address)
		connected = True
	#send the data
	sock.sendall(MSG.encode())
	#recieve and present the result
	byte_result = (sock.recv(1024)).decode()
	print (byte_result)
	paresed_res = json.loads(byte_result)
	status = paresed_res["status"]

	if status != "OK":
		print("Login/signup failed, try again \n")
	else:
		print("Login/signup successful!")

#after the login succeeded
while choice.upper() != "X":
	choice = input("CREATE MSG:\tchoose: \nR or r to get rooms \nP or p to get players in a room \nH or h to get highscores \nJ or j to join a room \nC or c to create a room \nX or x to signout\n" )

	if choice.upper() == "R" :
		#creating the msg:
		msgType = "R"
		#getRooms doesn't need any parameters
		MSG = msgType + str(0).zfill(4)
		print("sent:\t",MSG, "\n")

		sock.sendall(MSG.encode())
		byte_result = (sock.recv(1024)).decode()
		print("Recieved: ", byte_result, "\n")
		paresed_res = json.loads(byte_result)
		print ("Status: ", paresed_res["status"], "\n")
		print ("ID: ", paresed_res["roomsid"], "\n")
		print("Names: ", paresed_res["roomsnames"], "\n")
		print("Current users: ", paresed_res["roomscurrusers"], "\n")
		print("Max users: ", paresed_res["roomsmaxusers"], "\n")
		

	elif choice.upper() == "P":
		#creating the msg:
		msgType = "P"
		roomId = input("Enter the room's ID: ")
		data = "{\"roomid\" : " + roomId + "}"
		MSG = msgType + str(len(data)).zfill(4) + data
		print("sent:\t",MSG, "\n")

		sock.sendall(MSG.encode())
		byte_result = (sock.recv(1024)).decode()
		print("Recieved: ", byte_result, "\n")
		paresed_res = json.loads(byte_result)
		print("Players: ", paresed_res["players"], "\n")

	elif choice.upper() == "H":
		#creating the msg:
		msgType = "H"
		#no need for any further data
		MSG = msgType + str(0).zfill(4)
		print("sent:\t",MSG, "\n")
		
		sock.sendall(MSG.encode())
		byte_result = (sock.recv(1024)).decode()
		print("Recieved: ", byte_result, "\n")
		paresed_res = json.loads(byte_result)
		print("Status: ", paresed_res["status"], "\n")
		print("Highscores: ", paresed_res["highscores"], "\n")

	elif choice.upper() == "J":
		#creating the msg:
		msgType = "J"
		roomId = input("Enter the room's ID: ")
		data = "{\"roomid\" : " + roomId + "}"
		MSG = msgType + str(len(data)).zfill(4) + data
		print("sent:\t",MSG, "\n")

		sock.sendall(MSG.encode())
		byte_result = (sock.recv(1024)).decode()
		print("Recieved: ", byte_result, "\n")
		paresed_res = json.loads(byte_result)
		print("Status: ", paresed_res["status"], "\n")

	elif choice.upper() == "C":
		#creating the msg:
		msgType = "C"
		#this type of request has a bunch of parameters
		roomName = input("Enter the room's name: ")
		maxUsers = input("Enter the maximum amount of users: ")
		questionCount = input("Enter amount of questions in the room: ")
		questionTimeout = input("Enter the amount of time every question has: ")
		data = "{\"roomname\" : \""+roomName+"\", \"maxusers\" : " + maxUsers + ", \"questioncount\" : " + questionCount + ", \"questiontimeout\" : " + questionTimeout + "}"
		MSG = msgType + str(len(data)).zfill(4) + data
		print("sent:\t",MSG, "\n")

		sock.sendall(MSG.encode())
		byte_result = (sock.recv(1024)).decode()
		print("Recieved: ", byte_result, "\n")
		paresed_res = json.loads(byte_result)
		print("Status: ", paresed_res["status"], "\n")

	elif choice.upper() == "X":
		#creating the msg:
		msgType = "X"
		#this type of request has a bunch of parameters
		MSG = msgType + str(0).zfill(4)
		print("sent:\t",MSG, "\n")

		sock.sendall(MSG.encode())
		byte_result = (sock.recv(1024)).decode()
		print("Recieved: ", byte_result, "\n")
		paresed_res = json.loads(byte_result)
		print("Status: ", paresed_res["status"], "\n")
	
	else:
		print("Invalid input - wrong messege type \n")

	if choice.upper() == "C" and paresed_res["status"] == "OK":
		#the admin started a room
		while choice.upper() != "O" and choice.upper() != "X":
			choice = input("CREATE MSG:\tchoose: \nG or g to start the game \nO or o to close the room \nT or t to get the room state \nX or x to signout\n" )
		
			if choice.upper() == "G" or choice.upper() == "O" or choice.upper() == "T" or choice.upper() == "X":
				#creating the msg:
				msgType = choice.upper()
				MSG = msgType + str(0).zfill(4)
				print("sent:\t",MSG, "\n")
				#none of the admin requests require any parameters
				sock.sendall(MSG.encode())
				byte_result = (sock.recv(1024)).decode()
				print("Recieved: ", byte_result, "\n")
				paresed_res = json.loads(byte_result)

				print ("Status: ", paresed_res["status"], "\n")
				if choice.upper() == "T":
					print ("Has the game begun: ", paresed_res["hasgamebegun"], "\n")
					print ("Is the admin in the room: ", paresed_res["isadmininroom"], "\n")
					print("Players: ", paresed_res["players"], "\n")
					print("Question count: ", paresed_res["questioncount"], "\n")
					print("Question timeout: ", paresed_res["questiontimeout"], "\n")

			else:
				print("Invalid input - wrong message type \n")

	elif choice.upper() == "J" and paresed_res["status"] == "OK":
    	#the user has joined a room
		while not (choice.upper() == "E" and choice.upper() == "X") and not (choice.upper() == "T" and paresed_res["isadmininroom"] == False):
    		#if the admin left the room all members must leave as well
			choice = input("CREATE MSG:\tchoose: \nT or t to get the room state \nE or e to leave to room \nX or x to signout\n" )
		
			if choice.upper() == "T" or choice.upper() == "E" or choice.upper() == "X":
				#creating the msg:
				msgType = choice.upper()
				MSG = msgType + str(0).zfill(4)
				print("sent:\t",MSG, "\n")
				#none of the member requests require any parameters
				sock.sendall(MSG.encode())
				byte_result = (sock.recv(1024)).decode()
				print("Recieved: ", byte_result, "\n")
				paresed_res = json.loads(byte_result)

				print ("Status: ", paresed_res["status"], "\n")

				if choice.upper() == "T":
					print ("Has the game begun: ", paresed_res["hasgamebegun"], "\n")
					print ("Is the admin in the room: ", paresed_res["isadmininroom"], "\n")
					print("Players: ", paresed_res["players"], "\n")
					print("Question count: ", paresed_res["questioncount"], "\n")
					print("Question timeout: ", paresed_res["questiontimeout"], "\n")

			else:
				print("Invalid input - wrong message type \n")
    		
		


# Closing the socket
sock.close()

input()

