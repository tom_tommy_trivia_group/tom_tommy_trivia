﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Newtonsoft.Json;

namespace Trivia_Client
{
    /// <summary>
    /// Interaction logic for GetRoomsScreen.xaml
    /// </summary>
    /// 
    public partial class GetRoomsScreen : Window
    {
        public ClientData ClientData { get; set; }

        public GetRoomsScreen(ClientData clientData)
        {
            List<RoomData> rd = new List<RoomData>();

            this.ClientData = clientData;

            InitializeComponent();
            GetRoomsResponse res;
            lock (ClientData)
            {
                res = this.ClientData.SendAndReceive<GetRoomsResponse>('R', string.Empty);
            }
            this.RoomGrid.ItemsSource = res.TurnDictsIntoRoomDataList();
        }

        private void JoinButton_Click(object sender, RoutedEventArgs e)
        {
            //try to join a room
            RoomData roomData = (RoomData)this.RoomGrid.SelectedItem;
            if (roomData != null)
            {//if the roomdata is null then the grid is empty/the client didnt choose any room

                //create the msg
                JoinRoomRequest req = new JoinRoomRequest{ roomid = roomData.getId()};

                string data = JsonConvert.SerializeObject(req);

                var res = this.ClientData.SendAndReceive<JoinRoomResponse>('J', data);

                if (res.status == "OK")
                {//if the use joined the room
                    this.Close();
                    MemberRoomScreen wnd = new MemberRoomScreen(ClientData);
                    wnd.ShowDialog();
                }
                else
                {
                    MessageBox.Show(res.status, "Join Room Problem", MessageBoxButton.OK, MessageBoxImage.Hand);
                }

            }
            else
            {
                MessageBox.Show("You have to choose a room first", "Join Room Problem", MessageBoxButton.OK, MessageBoxImage.Hand);
            }
        }

        private void BackButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
            MenuScreen wnd = new MenuScreen(this.ClientData);
            wnd.ShowDialog();
        }
    }
}
