﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Newtonsoft.Json;

namespace Trivia_Client
{
    /// <summary>
    /// Interaction logic for HighscoreScreen.xaml
    /// </summary>
    public partial class HighscoreScreen : Window
    {
        public List<HighscoreRecord> hd = new List<HighscoreRecord> { new HighscoreRecord { Name = "Tom", Highscore = 58 }, new HighscoreRecord { Name = "Idan Shopin", Highscore = 9000 } };

        public ClientData ClientData { get; set; }

        public HighscoreScreen(ClientData userData)
        {

            this.ClientData = userData;
            InitializeComponent();

            //get all the highscores from the server

            var res = this.ClientData.SendAndReceive<HighscoreResponse>('H', string.Empty);
            if (res.status == "OK")
            {
                this.HighscoreGrid.ItemsSource = res.TurnResponseToRecordList();
            }
            else
            {
                MessageBox.Show(res.status, "Highscores Problem", MessageBoxButton.OK, MessageBoxImage.Error);
                this.Close();
            }
            
        }

        private void Back_Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
