﻿namespace Trivia_Client
{
    using System.Collections.Generic;

    /// <summary>
    /// used to communicate with the server
    /// </summary>
    internal class HighscoreResponse
    {
        public string status;
        public Dictionary<string, int> highscores;

        public List<HighscoreRecord> TurnResponseToRecordList()
        {
            List<HighscoreRecord> hr = new List<HighscoreRecord>();

            foreach (var record in highscores)
            {
                hr.Add(new HighscoreRecord { Name = record.Key, Highscore = record.Value });
            }

            return hr;
        }
    }
}
