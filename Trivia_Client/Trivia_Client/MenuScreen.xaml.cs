﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Newtonsoft.Json;


namespace Trivia_Client
{
    /// <summary>
    /// Interaction logic for MenuScreen.xaml
    /// </summary>
    public partial class MenuScreen : Window
    {
        public ClientData ClientData { get; set; }

        public MenuScreen(ClientData clientData)
        {
            this.ClientData = clientData;
            InitializeComponent();
        }

        private void Create_Room_Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
            CreateRoomScreen wnd = new CreateRoomScreen(this.ClientData);
            wnd.ShowDialog();
        }

        private void Get_Rooms_Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
            GetRoomsScreen wnd = new GetRoomsScreen(this.ClientData);
            wnd.ShowDialog();
        }

        private void Highscore_Button_Click(object sender, RoutedEventArgs e)
        {
            HighscoreScreen wnd = new HighscoreScreen(this.ClientData);
            wnd.ShowDialog();
        }

        private void Logout_Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
