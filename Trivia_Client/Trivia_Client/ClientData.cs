﻿namespace Trivia_Client
{
    using System.Net;
    using System.Net.Sockets;
    using System.Text;
    using Newtonsoft.Json;

    //class helps to pass information about the user form one window to another
    public class ClientData
    {
        public bool UserInServerMap { get; set; }//if the user has established connection with server
        public Socket Client_socket { get; set; }

        public void ConnectToServer()
        {
            IPHostEntry ipHost = Dns.GetHostEntry(Dns.GetHostName());
            IPEndPoint localEndPoint = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 8876);

            //connect with the server
            if (!this.UserInServerMap)
            {
                //the user is connected to the server (even if he isnt logged in yet)
                this.Client_socket.Connect(localEndPoint);
                this.UserInServerMap = true;
            }
        }

        //function recieves a string msg and writes to the byte array the result that the server gave
        public void SendAndReceive(string msg, byte[] byteRes)
        {
            // log the user into the server
            byte[] byteMsg = Encoding.ASCII.GetBytes(msg);
            this.Client_socket.Send(byteMsg);

            // receive msg from server
            this.Client_socket.Receive(byteRes);
        }

        //function recieves msg type (prefix) and data and returns an deserialized object that contains the server's response
        public T SendAndReceive<T>(char prefix, string data)
        {
            ///msg type + msgLen + msgData
            string msg = prefix + data.Length.ToString().PadLeft(4, '0') + data;
            byte[] byteRes = new byte[1024];
            this.SendAndReceive(msg, byteRes);
            return JsonConvert.DeserializeObject<T>(Encoding.ASCII.GetString(byteRes));
        }
    }
}
