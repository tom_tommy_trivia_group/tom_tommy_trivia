﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Trivia_Client
{
    /// <summary>
    /// used to communicate with the server
    /// </summary>
    class CreateRoomRequest
    {
            public string roomname;
            public int maxusers;
            public int questioncount;
            public int questiontimeout;
    }
}
