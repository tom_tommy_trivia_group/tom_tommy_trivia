﻿namespace Trivia_Client
{
    /// <summary>
    /// Showcase each of  the Highscores records that were received in Highscores request.
    /// </summary>
    public class HighscoreRecord
    {
        public string Name { get; set; }
        public int Highscore { get; set; }
    }
}
