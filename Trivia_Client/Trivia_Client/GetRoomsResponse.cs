﻿namespace Trivia_Client
{
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// used to communicate with the server
    /// </summary>
    internal class GetRoomsResponse
    {
        public string status;
        public List<int> roomsid;
        public List<string> roomsnames;
        public List<int> roomscurrusers;
        public List<int> roomsmaxusers;

        internal List<RoomData> TurnDictsIntoRoomDataList()
        {
            List<RoomData> roomDataList = new List<RoomData>();
            for (int i = 0; i < roomsid.Count(); i++)
            {
                roomDataList.Add(new RoomData(roomsid.ElementAt(i), roomsnames.ElementAt(i), roomscurrusers.ElementAt(i), roomsmaxusers.ElementAt(i)));
            }

            return roomDataList;
        }
    }
}
