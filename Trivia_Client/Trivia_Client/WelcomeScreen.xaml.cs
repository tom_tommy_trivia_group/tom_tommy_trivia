﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Newtonsoft.Json;

namespace Trivia_Client
{
    /// <summary>
    /// Interaction logic for WelcomeScreen.xaml
    /// </summary>
    public partial class WelcomeScreen : Window
    {
        public WelcomeScreen(ClientData clientData)
        {
            this.ClientData = clientData;
            InitializeComponent();
        }

        public ClientData ClientData { get; set; }

        private void Login_Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
            LoginScreen wnd = new LoginScreen(this.ClientData);
            wnd.ShowDialog();
        }

        private void Signup_Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
            SignupScreen wnd = new SignupScreen(this.ClientData);
            wnd.ShowDialog();
        }
    }
}
