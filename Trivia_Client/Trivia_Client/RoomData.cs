﻿namespace Trivia_Client
{
    /// <summary>
    /// Saves and showcases each of the rooms that were received in get rooms query.
    /// </summary>
    public class RoomData
    {
        private readonly int ID;
        public string Name { get; private set; }
        public int CurrentPlayers { get; private set; }
        public int MaximumPlayers { get; private set; }

        public RoomData(int id, string name, int currPlayers, int maxPlayers)
        {
            this.ID = id;
            this.Name = name;
            this.CurrentPlayers = currPlayers;
            this.MaximumPlayers = maxPlayers;
        }

        internal int getId()
        {
            return this.ID;
        }
    }
}
