﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Newtonsoft.Json;


namespace Trivia_Client
{
    /// <summary>
    /// Interaction logic for SignupScreen.xaml
    /// </summary>
    public partial class SignupScreen : Window
    {
        public SignupScreen(ClientData clientData)
        {
            this.ClientData = clientData;
            InitializeComponent();
        }

        public ClientData ClientData { get; set; }

        private void Back_Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
            WelcomeScreen wnd = new WelcomeScreen(this.ClientData);
            wnd.ShowDialog();
        }

        private void Login_Button_Click(object sender, RoutedEventArgs e)
        {
            string errMsg = null;

            Utilities.AggregateErrorMsg(this.UsernameTxt.Text, ref errMsg, "Vessel! You lack a name!");
            Utilities.AggregateErrorMsg(this.PasswordTxt.Password, ref errMsg, "Vessel! You lack a password!");
            Utilities.AggregateErrorMsg(this.EmailTxt.Text, ref errMsg, "Vessel! You lack a nail!");
            
            if (errMsg == null)
            {

                try
                {
                    this.ClientData.ConnectToServer();
                }
                catch (SocketException se)
                {
                    MessageBox.Show(se.ToString(), "Client socket error", MessageBoxButton.OK, MessageBoxImage.Error);
                    this.Close();
                }

                //create the msg
                SignupRequest req = new SignupRequest();
                req.username = this.UsernameTxt.Text;
                req.password = this.PasswordTxt.Password;
                req.email = this.EmailTxt.Text;

                string data = JsonConvert.SerializeObject(req);

                var res = this.ClientData.SendAndReceive<SignupResponse>('S', data);

                if (res.status == "OK")
                {//if login was successful
                    Utilities.PaleKingQuote();
                    this.Close();
                    MenuScreen wnd = new MenuScreen(this.ClientData);
                    wnd.ShowDialog();
                }
                else
                {
                    MessageBox.Show(res.status, "Signup Problem", MessageBoxButton.OK, MessageBoxImage.Hand);
                }
            }
            else
            {
                MessageBox.Show(errMsg, "Missing Details", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
