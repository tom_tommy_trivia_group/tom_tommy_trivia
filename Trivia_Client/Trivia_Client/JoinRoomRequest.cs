﻿namespace Trivia_Client
{
    /// <summary>
    /// used to communicate with the server
    /// </summary>
    internal class JoinRoomRequest
    {
        public int roomid;
    }
}
