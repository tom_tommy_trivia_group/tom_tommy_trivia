﻿namespace Trivia_Client
{
    /// <summary>
    /// used to communicate with the server
    /// </summary>
    internal class SignupRequest
    {
        public string username;
        public string password;
        public string email;
    }
}
