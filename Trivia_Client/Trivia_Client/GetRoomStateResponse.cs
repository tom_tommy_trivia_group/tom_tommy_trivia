﻿namespace Trivia_Client
{

    using System;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// used to communicate with the server
    /// </summary>
    internal class GetRoomStateResponse
    {
        public string status;
        public bool hasgamebegun;
        public bool isadmininroom;
        public List<string> players;
        public int questioncount;
        public int questiontimeout;

        //function returns if the thread taht uses it can continue asking for the room's state
        internal bool CanContinueAskingForState()
        {
            bool canContinueAskingForState = false;

            if (this.hasgamebegun == false && this.isadmininroom == true)
            {
                canContinueAskingForState = true;
            }

            return canContinueAskingForState;
        }
    }
}
