﻿namespace Trivia_Client
{
    using System;
    using System.Windows;

    //a class that contains functions that are used by all/a few windows
    public class Utilities
    {
        //found about this thing below - it's very userful, you only need to tap '/' three times
        /// <summary>
        /// Verifies if a certain field is lacking data.
        /// </summary>
        /// <param name="field"></param> - the field that may be empty
        /// <param name="errorString"></param> - the error string that we will append to if the field is missing. It will be changed so it's a ref
        /// <param name="error"></param> - the string that will be appended if the field is empty
        public static void AggregateErrorMsg(string field, ref string errorString, string error)
        {
            if (string.IsNullOrEmpty(field))//checks if the field is empty
            {
                if (errorString != null)//if this is not the first time it will start a new line
                {
                    errorString += Environment.NewLine;
                }
                errorString += error;
            }
        }

        /// <summary>
        /// Checks if the data can be parsed into int type. if not it aggregates error msg.
        /// </summary>
        /// <param name="data">the string that we want to check</param>
        /// <param name="errorString">the string where the error will be aggregated into if the string content has the wrong type</param>
        /// <param name="error">the string that will be aggregated into errorString</param>
        /// <param name="result">the integer we try to parse the data into</param>
        public static void AggregateTypeErrorMsg(string data, ref string errorString, string error, ref int result)
        {
            if (!Int32.TryParse(data, out result))//checks if the field is empty
            {
                if (errorString != null)//if this is not the first time it will start a new line
                {
                    errorString += Environment.NewLine;
                }
                errorString += error;
            }
        }

        /// <summary>
        /// the function gives a spooky messege for the user
        /// </summary>
        public static void PaleKingQuote()
        {
            //these are referances from the game "Hollow Knight" (spoilers)
            MessageBox.Show("No Mind To Think", "No Mind To Think", MessageBoxButton.OK);
            MessageBox.Show("No Will To Break", "No Will To Break", MessageBoxButton.OK);
            MessageBox.Show("No Voice To Cry Suffering", "No Voice To Cry Suffering", MessageBoxButton.OK);
        }
    }
}
