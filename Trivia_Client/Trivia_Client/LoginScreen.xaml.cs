﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Newtonsoft.Json;

namespace Trivia_Client
{
    /// <summary>
    /// Interaction logic for LoginScreen.xaml
    /// </summary>
    /// 
    public partial class LoginScreen : Window
    {
        public LoginScreen(ClientData clientData)
        {
            this.ClientData = clientData;
            InitializeComponent();
        }

        public ClientData ClientData { get; private set; }

        private void Back_Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
            WelcomeScreen wnd = new WelcomeScreen(this.ClientData);
            wnd.ShowDialog();
        }

        private void Login_Button_Click(object sender, RoutedEventArgs e)
        {
            string errorString = null;

            Utilities.AggregateErrorMsg(this.UsernameTxt.Text, ref errorString, "Vessel! What is your name?");
            Utilities.AggregateErrorMsg(this.PasswordTxt.Password, ref errorString, "Vessel! What is your password?");

            if (errorString == null)
            {
                try
                {
                    this.ClientData.ConnectToServer();
                }
                catch (SocketException se)
                {
                    MessageBox.Show(se.ToString(), "Client socket error", MessageBoxButton.OK, MessageBoxImage.Error);
                    this.Close();
                }



                //create the msg
                LoginRequest req = new LoginRequest
                {
                    username = this.UsernameTxt.Text,
                    password = this.PasswordTxt.Password
                };

                string data = JsonConvert.SerializeObject(req);

                var res = this.ClientData.SendAndReceive<LoginResponse>('L', data);

                if (res.status == "OK")
                {//if login was successful
                    this.Close();
                    MenuScreen wnd = new MenuScreen(this.ClientData);
                    wnd.ShowDialog();
                }
                else
                {
                    MessageBox.Show(res.status, "Login Problem", MessageBoxButton.OK, MessageBoxImage.Hand);
                }

            }
            else
            {
                MessageBox.Show(errorString, "Missing Details", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
