﻿namespace Trivia_Client
{
    /// <summary>
    /// used to communicate with the server
    /// </summary>
    internal class LoginRequest
    {
        public string username;
        public string password;
    }
}
