﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Newtonsoft.Json;

namespace Trivia_Client
{
    /// <summary>
    /// Interaction logic for CreateRoomScreen.xaml
    /// </summary>
    public partial class CreateRoomScreen : Window
    {
        public ClientData ClientData { get; set; }

        public CreateRoomScreen(ClientData clientData)
        {
            this.ClientData = clientData;
            InitializeComponent();
        }

        private void Submit_Button_Click(object sender, RoutedEventArgs e)
        {
            string errorStr = null;
            int maxUsers = 0;
            int questionCount = 0;
            int questionTimeout = 0;

            Utilities.AggregateErrorMsg(RoomNameTxt.Text, ref errorStr, "Your room has no name!");
            Utilities.AggregateErrorMsg(MaxUsersTxt.Text, ref errorStr, "Your room has no player limits!");
            Utilities.AggregateErrorMsg(QuestionCountTxt.Text, ref errorStr, "Your room has no amount of questions!");
            Utilities.AggregateErrorMsg(QuestionTimeoutTxt.Text, ref errorStr, "Your room's question has no defiend time!");

            if (errorStr == null)
            {

                Utilities.AggregateTypeErrorMsg(MaxUsersTxt.Text, ref errorStr, "The player limit must be a number!", ref maxUsers);
                Utilities.AggregateTypeErrorMsg(QuestionCountTxt.Text, ref errorStr, "The question amount must be a number!", ref questionCount);
                Utilities.AggregateTypeErrorMsg(QuestionTimeoutTxt.Text, ref errorStr, "The question time limit must be a number!", ref questionTimeout);

                if(errorStr == null)
                {
                    //initiate the create room request
                    CreateRoomRequest req = new CreateRoomRequest { roomname = RoomNameTxt.Text, maxusers = maxUsers, questioncount = questionCount, questiontimeout = questionTimeout };

                    string data = JsonConvert.SerializeObject(req);

                    var res = this.ClientData.SendAndReceive<CreateRoomResponse>('C', data);

                    if (res.status == "OK")
                    {//if the room creation was successful
                        this.Close();
                        AdminRoomScreen wnd = new AdminRoomScreen(ClientData);
                        wnd.ShowDialog();
                    }
                    else
                    {
                        MessageBox.Show(res.status, "Create Room Problem", MessageBoxButton.OK, MessageBoxImage.Hand);
                    }
                }
                else
                {
                    MessageBox.Show(errorStr, "Incorrect Field Types", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            else
            {
                MessageBox.Show(errorStr, "Missing Details", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void Clear_Button_Click(object sender, RoutedEventArgs e)
        {
            RoomNameTxt.Text = string.Empty;
            MaxUsersTxt.Text = string.Empty;
            QuestionCountTxt.Text = string.Empty;
            QuestionTimeoutTxt.Text = string.Empty;
        }

        private void Back_Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
            MenuScreen wnd = new MenuScreen(this.ClientData);
            wnd.ShowDialog();
        }
    }
}
