﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Trivia_Client
{
    /// <summary>
    /// Interaction logic for AdminRoomScreen.xaml
    /// </summary>
    public partial class AdminRoomScreen : Window
    {
        //private static Mutex mut = new Mutex();
        private bool _wasClosed = false;

        public ClientData ClientData { get; set; }//has the socket and data about the client

        public AdminRoomScreen(ClientData clientData)
        {
            this.ClientData = clientData;
            InitializeComponent();

            //make the function run in parallel
            Task.Run((Action) UpdateScreen);
        }

        private void Start_Game_Button_Click(object sender, RoutedEventArgs e)
        {
            StartGameResponse result;

            lock (ClientData)
            {
                //send empty string because that request has no data in it
                result = this.ClientData.SendAndReceive<StartGameResponse>('G', "");
            }

            this.Close();
            FutureFeaturesScreen wnd = new FutureFeaturesScreen();
            wnd.ShowDialog();
        }

        private void Close_Room_Button_Click(object sender, RoutedEventArgs e)
        {
            CloseRoomResponse result;

            lock (ClientData)
            {
                //send empty string because that request has no data in it
                result = this.ClientData.SendAndReceive<CloseRoomResponse>('O', "");
            }

            this.Close();
            _wasClosed = true;
            GetRoomsScreen wnd = new GetRoomsScreen(ClientData);
            wnd.ShowDialog();

        }

        //function send the getRoomData request to the server in order to get updates from it
        private void UpdateScreen()
        {
            do
            {
                GetRoomStateResponse result;
                //this acts like amutex
                lock (ClientData)
                {
                    if(_wasClosed)
                    {
                        break;
                    }
                    //send empty string because that request has no data in it
                    result = this.ClientData.SendAndReceive<GetRoomStateResponse>('T', "");
                }

                this.Dispatcher.Invoke(() => this.PLayerGrid.ItemsSource = result.players);
                bool canContinueSendingMesseges = result.CanContinueAskingForState();

                //check if you can continue
                if (!canContinueSendingMesseges)
                {
                    break;
                }

                //wait 3 seconds untill the next check in order to not overload the server
                Thread.Sleep(3000);
            }
            while (!_wasClosed);
        }
    }
}
