﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Trivia_Client
{
    /// <summary>
    /// Interaction logic for MemberRoomScreen.xaml
    /// </summary>
    public partial class MemberRoomScreen : Window
    {
        public ClientData ClientData { get; set; }//has the socket and data about the client
        private bool _wasClosed = false;

        public MemberRoomScreen(ClientData clientData)
        {
            this.ClientData = clientData;
            InitializeComponent();

            Task.Run((Action) UpdateScreen);
        }

        private void Leave_Room_Button_Click(object sender, RoutedEventArgs e)
        {
            LeaveRoomResponse result;

            lock (ClientData)
            {
                //send empty string because that request has no data in it
                result = this.ClientData.SendAndReceive<LeaveRoomResponse>('E', "");
            }

            this.Close();
            _wasClosed = true;
            GetRoomsScreen wnd = new GetRoomsScreen(ClientData);
            wnd.ShowDialog();
        }

        //function send the getRoomData request to the server in order to get updates from it
        private void UpdateScreen()
        {
            GetRoomStateResponse result = null;

            do
            {

                //this acts like amutex
                lock (ClientData)
                {
                    if (_wasClosed)
                    {
                        break;
                    }
                    //send empty string because that request has no data in it
                    result = this.ClientData.SendAndReceive<GetRoomStateResponse>('T', "");
                }

                this.PLayerGrid.ItemsSource = result.players;
                bool canContinueSendingMesseges = result.CanContinueAskingForState();
                //check if you can continue
                if (!canContinueSendingMesseges)
                {
                    break;
                }
                //wait 3 seconds untill the next check in order to not overload the server
                Thread.Sleep(3000);
            }
            while (!_wasClosed);

            this.Close();
            Window wnd;
            //if the game has begun
            if (result.hasgamebegun)
            {
                wnd = new FutureFeaturesScreen();
            }
            else
            {
                wnd = new GetRoomsScreen(ClientData);
            }
            wnd.ShowDialog();
        }
    }
}
