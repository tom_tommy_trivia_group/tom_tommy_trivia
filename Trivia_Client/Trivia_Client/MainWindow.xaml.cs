﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Newtonsoft.Json;

namespace Trivia_Client
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public ClientData ClientData { get; private set; }

        public MainWindow()
        {
            InitializeComponent();
            bool connectionSuccess = true;

            this.ClientData = new ClientData
            {
                UserInServerMap = false
            };


            try
            {
                //initiating the socket
                this.ClientData.Client_socket = new Socket(AddressFamily.InterNetwork,
                       SocketType.Stream, ProtocolType.Tcp);

            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString(), "Unknown error", MessageBoxButton.OK, MessageBoxImage.Error);
                connectionSuccess = false;
            }



            if(connectionSuccess)//if the connection was successful
            {
                WelcomeScreen wnd = new WelcomeScreen(this.ClientData);
                wnd.ShowDialog();

                //after all the dialog is over, check if client is connected to the server
                if (this.ClientData.UserInServerMap)
                {
                    //logout
                    byte[] byteRes = new byte[1024];

                    int len = 0;
                    string msg = "X" + len.ToString().PadLeft(4, '0');

                    this.ClientData.SendAndReceive(msg, byteRes);

                    //shut down the connection
                    this.ClientData.Client_socket.Shutdown(SocketShutdown.Both);
                }
            }


            //close the socket we opened at the beginning of the dialog
            this.ClientData.Client_socket.Close();
            this.Close();
        }
    }
}