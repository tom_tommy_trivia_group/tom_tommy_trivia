#include "Room.h"

//constructor
Room::Room(const RoomData& data) : m_metadata(data)
{
}

//destructor
Room::~Room()
{
	int i;

	for (i = 0; i < this->m_users.size(); i++)
	{
		this->m_users[i].reset();
	}

	this->m_users.clear();
}

//functions adds user to the game. if the user is already in the game it won't add it
void Room::addUser(std::shared_ptr<const LoggedUser> newUser)
{

	this->m_users.push_back(newUser);
}

//removes a user
bool Room::removeUser(std::shared_ptr<const LoggedUser> user)
{
	for (auto it = this->m_users.begin(); it != this->m_users.end(); ++it)
	{
		if ((*it)->getUsername() == user->getUsername())
		{
			//this->m_users[i].reset(); - if the line below doesnt work
			(*it).reset();
			this->m_users.erase(it);
			return true;
		}
	}

	return false;
}

//returns all users
std::vector<std::shared_ptr<const LoggedUser>> Room::getAllUsers()
{
	return this->m_users;
}

//returns the room's data
RoomData Room::getData()
{
	return this->m_metadata;
}

bool Room::isRoomFull()
{
	bool isRoomFull = false;

	if (this->m_users.size() == this->m_metadata.m_maxPlayers)
	{
		isRoomFull = true;
	}

	return isRoomFull;
}

void Room::activate()
{
	this->m_metadata.isActive = true;
}

//turns the isAdminInRoom property to false
void Room::adminLeft()
{
	this->m_metadata.isAdminInRoom = false;
}