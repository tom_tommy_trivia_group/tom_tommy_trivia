this folder contains all the files that manage some aspects of the server, like users or rooms,
and the classes that support them.

V100:
		LoggedUser
		LoginManager
V200:
		RoomData
		Room
		RoomManager
		HighscoreTable
V300:
		//TBD
V400:
		//TDB
