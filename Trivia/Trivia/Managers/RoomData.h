#pragma once
#include <string>
struct RoomData
{
public:
	RoomData(unsigned int id = 0, std::string name = "", unsigned int maxPlayers = 0, unsigned int questionCount = 0, unsigned int timePerQuestion = 0);//constructor
	RoomData(const RoomData& other);//copy constructor
	~RoomData();//destructor


	unsigned int m_id;
	std::string m_name;
	unsigned int m_maxPlayers;
	unsigned int m_questionCount;
	unsigned int m_timePerQustion;
	bool isActive;
	bool isAdminInRoom;
};