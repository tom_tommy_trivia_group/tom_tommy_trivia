#pragma once
#include "LoggedUser.h"
#include "..\RequestIdProtocols.h"
#include "..\SQLiteDatabase.h"
#include <vector>
#include <memory>

class LoginManager
{
public:
	LoginManager(std::shared_ptr<SQLiteDatabase> db);//constructor
	~LoginManager();//destructor
	std::shared_ptr<SQLiteDatabase> getDb();//getter
	LoginManager& operator= (LoginManager& other);//this operator copies the values from one logom manger to another
	unsigned int login(std::string username, std::string pass);//logins the user into the database
	unsigned int signup(std::string username, std::string pass, std::string email);//signs the user into the database
	void logout(std::string username);//logs the user out

	//getter
	std::shared_ptr<LoggedUser> getUser(std::string username);
private:
	std::shared_ptr<SQLiteDatabase> m_db;
	std::vector<std::shared_ptr<LoggedUser>> m_loggerUsers;
};