#include "RoomManager.h"

//cinstructor
RoomManager::RoomManager(std::map<unsigned int, std::shared_ptr<Room>> curr_rooms) : m_rooms(curr_rooms)
{
}

//default constructor
RoomManager::RoomManager()
{
}

//destructor
RoomManager::~RoomManager()
{
	for (auto it = this->m_rooms.begin(); it != this->m_rooms.end(); ++it)
	{
		it->second.reset();//remove the element within the shared_ptr
	}

	this->m_rooms.clear();
}

//function creates a room with the admin
unsigned int RoomManager::createRoom(std::shared_ptr<const LoggedUser> admin, std::string roomName, unsigned int maxUsers, unsigned int questionCount, unsigned int questionTimeout)
{
	unsigned int id, status = STATUS_OK;

	if (this->m_rooms.empty())
	{//if this is the first room
		id = 1;
	}
	else
	{//add 1 to the id of the last existing room (with the highes id value)
		id = this->m_rooms.rbegin()->first + 1;

		for (auto it = this->m_rooms.begin(); it != this->m_rooms.end(); ++it)
		{
			if (it->second->getData().m_name == roomName)
			{//if there is a room with the same name
				
				status = CREATE_ROOM_ROOM_NAME_IS_USED;
			}
		}
	}

	
	if (status == STATUS_OK)
	{
		this->m_rooms.insert(std::pair<unsigned int, std::shared_ptr<Room>>(id, std::shared_ptr<Room>(new Room(*(new RoomData(id, roomName, maxUsers, questionCount, questionTimeout))))));
		this->m_rooms.at(id)->addUser(admin);
	}
	

	return status;
}

//the function deletes the wanted room
bool RoomManager::deleteRoom(unsigned int id)
{
	bool wasDeleted = false;

	for (auto it = this->m_rooms.begin(); it != this->m_rooms.end() && !wasDeleted; it++)//going over the rooms
	{
		if (it->first == id)
		{
			it->second.reset();;
			this->m_rooms.erase(it);
			return true;
		}
	}

	return wasDeleted;
}

//returns the state of the room
bool RoomManager::getRoomState(unsigned int id)
{
	if (this->m_rooms.count(id) == 0)//if the room is nonexistant
	{
		return false;
	}
	else
	{
		return this->m_rooms.at(id)->getData().isActive;
	}
}

//function retuns all the rooms
std::vector<std::shared_ptr<Room>> RoomManager::getRooms()
{
	std::vector<std::shared_ptr<Room>> rooms;
	for (auto it = this->m_rooms.begin(); it != this->m_rooms.end(); ++it)
	{
		rooms.push_back(it->second);
	}

	return rooms;
}

//function gets a name and returns the room that has that name. if no room has that name it returns nullptr
std::shared_ptr<Room> RoomManager::getRoomByName(std::string name)
{
	for (auto it = this->m_rooms.begin(); it != this->m_rooms.end(); ++it)
	{
		if ((*it).second->getData().m_name == name)
		{
			return (*it).second;
		}
	}

	return nullptr;
}

//function gets roomd id returns the room that has that has that id. if no room has that id it returns nullptr
std::shared_ptr<Room> RoomManager::getRoomById(unsigned int id)
{
	if (this->m_rooms.count(id) != 0)
	{
		return this->m_rooms.at(id);
	}

	return nullptr;
}