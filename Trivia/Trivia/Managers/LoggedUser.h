#pragma once
#include <iostream>
#include <string>

class LoggedUser
{
private:
	std::string m_usrename;
public:
	LoggedUser(std::string username);//constructor
	~LoggedUser();//destructor
	std::string getUsername() const;//getter
};