#include "RoomData.h"

//constructor
RoomData::RoomData(unsigned int id, std::string name, unsigned int maxPlayers, unsigned int questionCount, unsigned int timePerQuestion) : m_id(id), m_name(name), m_maxPlayers(maxPlayers), m_questionCount(questionCount), m_timePerQustion(timePerQuestion), isActive(false), isAdminInRoom
(true)
{
}

//copy constructor
RoomData::RoomData(const RoomData& other)
{
	this->m_id = other.m_id;
	this->m_maxPlayers = other.m_maxPlayers;
	this->m_name = other.m_name;
	this->m_questionCount = other.m_questionCount;
	this->m_timePerQustion = other.m_timePerQustion;
	this->isActive = other.isActive;
	this->isAdminInRoom = other.isAdminInRoom;
}

//destructor
RoomData::~RoomData()
{

}