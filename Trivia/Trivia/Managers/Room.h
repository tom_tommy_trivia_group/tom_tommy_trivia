#pragma once
#include <vector>
#include "RoomData.h"
#include "LoggedUser.h"
#include <memory>


class Room
{
public:
	Room(const RoomData& data);//constructor
	~Room();//destructor

	void addUser(std::shared_ptr<const LoggedUser>);//the funcion add the user to the room
	bool removeUser(std::shared_ptr<const LoggedUser>);//the funcion removes the user from the room
	std::vector<std::shared_ptr<const LoggedUser>> getAllUsers();//the function returns all the users that are in the room
	void activate();//turn the "HasGameStarted" on
	void adminLeft();//turns the "isAdminInRoom property to false

	bool isRoomFull();//returns if the room is full

	RoomData getData();

private:
	RoomData m_metadata;
	std::vector<std::shared_ptr<const LoggedUser>> m_users;
};