#include "LoginManager.h"

//constructor
LoginManager::LoginManager(std::shared_ptr<SQLiteDatabase> db) : m_db(db)
{
}

//destructor
LoginManager::~LoginManager()
{
	this->m_db.reset();
	std::vector<std::shared_ptr<LoggedUser>> &users = this->m_loggerUsers;
	for (auto it = users.begin(); it != users.end(); ++it)
	{//reseting each of the shared ptrs in the user list
		it->reset();
	}

	users.clear();
}

//database getter
std::shared_ptr<SQLiteDatabase> LoginManager::getDb()
{
	return this->m_db;
}

//the function puts the user in the logged users list if they're signed up
unsigned int LoginManager::login(std::string username, std::string pass)
{
	unsigned int loginStatus = STATUS_OK;
	
	for (auto it = this->m_loggerUsers.begin(); it != this->m_loggerUsers.end(); ++it)
	{
		if ((*it)->getUsername() == username)//if there is already a user with the same name
		{
			loginStatus = LOGIN_USER_ALREADY_LOGGED;
		}
	}
	if (loginStatus == STATUS_OK)//if no user is logged
	{
		if (this->m_db->isUserSigned(username, pass))
		{
			this->m_loggerUsers.push_back(std::shared_ptr<LoggedUser>(new LoggedUser(username)));
		}
		else
		{
			loginStatus = LOGIN_USER_ISNT_IN_THE_DB;
		}
	}

	return loginStatus;
}

//the function signs up the user into the database
unsigned int LoginManager::signup(std::string username, std::string pass, std::string email)
{
	bool signupSuccess;
	unsigned int signupStatus;
	signupSuccess = this->m_db->signup(username, pass, email);

	if (signupSuccess)
	{
		signupStatus = STATUS_OK;
	}
	else
	{
		signupStatus = SIGNUP_USER_ALREADY_IN_DB;
	}

	return signupStatus;
}

//function removes a user from the user list
void LoginManager::logout(std::string username)
{
	for (auto it = this->m_loggerUsers.begin(); it != this->m_loggerUsers.end(); ++it)
	{
		if ((*it)->getUsername() == username)
		{
			it->reset();
			this->m_loggerUsers.erase(it);
			break;
		}
	}
}

//operator puts the values of the fields in a different manager into this
LoginManager& LoginManager::operator= (LoginManager& other)
{
	this->m_db = other.getDb();
	return *(this);
}

//function gets a wanted user
std::shared_ptr<LoggedUser> LoginManager::getUser(std::string username)
{
	for (auto it = this->m_loggerUsers.begin(); it != this->m_loggerUsers.end(); ++it)
	{
		if ((*it)->getUsername() == username)
		{
			return (*it);
		}
	}

	return nullptr;
}