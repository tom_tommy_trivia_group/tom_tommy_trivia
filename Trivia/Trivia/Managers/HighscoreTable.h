#pragma once
#include "..\SQLiteDatabase.h"
#include <memory>

class HighscoreTable
{
public:
	HighscoreTable(std::shared_ptr<SQLiteDatabase> db);//constructor
	~HighscoreTable();//destructor
	std::map<std::string, int> getHighscores();//gets all the highscores fron the db
private:
	std::shared_ptr<SQLiteDatabase> m_database;//the database
};