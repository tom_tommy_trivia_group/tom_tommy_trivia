#include "HighscoreTable.h"

//constructor
HighscoreTable::HighscoreTable(std::shared_ptr<SQLiteDatabase> db) : m_database(db)
{
}

//destructor
HighscoreTable::~HighscoreTable()
{
}

//function returns the highscores as a map with descending score values
std::map<std::string, int> HighscoreTable::getHighscores()
{
	return this->m_database->getHisgscores();
}