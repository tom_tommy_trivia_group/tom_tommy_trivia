#pragma once
#include <map>
#include "Room.h"
#include "..\RequestIdProtocols.h"

class RoomManager
{
public:
	RoomManager(std::map<unsigned int, std::shared_ptr<Room>> curr_rooms);//constructor
	RoomManager();//default constructor
	~RoomManager();

	unsigned int createRoom(std::shared_ptr<const LoggedUser> admin, std::string roomName, unsigned int maxUsers, unsigned int questionCount, unsigned int questionTimeout);//create a room when all roomData is known
	bool deleteRoom(unsigned int id);
	bool getRoomState(unsigned int id);
	std::vector<std::shared_ptr<Room>> getRooms();
	std::shared_ptr<Room> getRoomByName(std::string name);
	std::shared_ptr<Room> getRoomById(unsigned int id);

private:
	std::map<unsigned int, std::shared_ptr<Room>> m_rooms;
};