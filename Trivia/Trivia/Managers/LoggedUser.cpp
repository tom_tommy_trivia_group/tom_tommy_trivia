#include "LoggedUser.h"

//constructor
LoggedUser::LoggedUser(std::string username)
{
	this->m_usrename = username;
}

//destructor
LoggedUser::~LoggedUser()
{

}

//getter
std::string LoggedUser::getUsername() const
{
	return this->m_usrename;
}