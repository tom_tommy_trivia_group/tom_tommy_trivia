#include "JsonResponsePacketSerializer.h"
//just for printing
#include <iostream>

//function gets a enum status value and returns string that has the enum's meaning
std::string statusToString(unsigned int status)
{
	std::string stringStatus;

	switch (status)
	{
		case STATUS_OK:
			stringStatus = "OK";
			break;

		case SIGNUP_USER_ALREADY_IN_DB:
			stringStatus = "This user (username or email) already exists";
			break;

		case LOGIN_USER_ISNT_IN_THE_DB:
			stringStatus = "This user doesn't exists";
			break;

		case LOGIN_USER_ALREADY_LOGGED:
			stringStatus = "This user is already logged in";
			break;

		case JOIN_ROOM_ROOM_NOT_FOUND:
			stringStatus = "Room not found";
			break;

		case JOIN_ROOM_ROOM_IS_FULL:
			stringStatus = "Room is full";
			break;

		case CREATE_ROOM_ROOM_NAME_IS_USED:
			stringStatus = "The name of the romm is already taken";
			break;

		case GET_ROOM_STATE_ROOM_WAS_CLOSED:
			stringStatus = "The room was closed";
			break;

		case STATUS_UNKNOWN_ERROR:
			stringStatus = "Unknown error";
			break;

		default:
			std::cout << "ERROR - Status code doesn't exists";
			stringStatus = "Unknown error";
			break;
	}

	return stringStatus;
}

/*
making a json msg from this response
input: the response
outpt: the msg
*/
std::string JsonResponsePacketSerializer::serializeResponse(ErrorResponse response)
{
	json jMsg;
	jMsg[ERROR_MESSEGE] = response.message;
	return jMsg.dump();
}

/*
making a json msg from this response
input: the response
outpt: the msg
*/
std::string JsonResponsePacketSerializer::serializeResponse(LoginResponse response)
{
	json jMsg;
	jMsg[LOGIN_STATUS] = statusToString(response.status);
	return jMsg.dump();
}

/*
making a json msg from this response
input: the response
outpt: the msg
*/
std::string JsonResponsePacketSerializer::serializeResponse(SignupResponse response)
{
	json jMsg;
	jMsg[SIGNUP_STATUS] = statusToString(response.status);
	return jMsg.dump();
}

/*
making a json msg from this response
input: the response
outpt: the msg
*/
std::string JsonResponsePacketSerializer::serializeResponse(CreateRoomResponse response)
{
	json jMsg;
	jMsg[CREATE_ROOM_STATUS] = statusToString(response.status);
	return jMsg.dump();
}

/*
making a json msg from this response
input: the response
outpt: the msg
*/
std::string JsonResponsePacketSerializer::serializeResponse(GetPlayersInRoomResponse response)
{
	json jMsg;
	jMsg[GET_PLAYERS_IN_ROOM_KEY] = response.players;
	return jMsg.dump();
}

/*
making a json msg from this response
input: the response
outpt: the msg
*/
std::string JsonResponsePacketSerializer::serializeResponse(GetRoomsResponse response)
{
	json jMsg;
	
	//contains the IDs of the rooms
	std::vector<unsigned int>roomsId;
	//contains the names of the rooms
	std::vector<std::string>roomsNames;
	//contains the current users amount of each room
	std::vector<unsigned int> roomsCurrUsers;
	//map contains id and current amount of users
	std::vector<unsigned int> roomsMaxUsers;
	for (auto it = response.rooms.begin(); it != response.rooms.end(); ++it)
	{/*i need to put all the room names in a special container because json can't
		dump a custom class*/

		//gets the room's name
		roomsId.push_back((*it)->getData().m_id);
		roomsNames.push_back((*it)->getData().m_name);
		roomsCurrUsers.push_back((*it)->getAllUsers().size());
		roomsMaxUsers.push_back((*it)->getData().m_maxPlayers);

	}
	jMsg[GET_ROOMS_STATUS] = statusToString(response.status);
	jMsg[GET_ROOMS_ID] = roomsId;
	jMsg[GET_ROOMS_NAMES] = roomsNames;
	jMsg[GET_ROOMS_CURR_USERS] = roomsCurrUsers;
	jMsg[GET_ROOMS_MAX_USERS] = roomsMaxUsers;
	std::cout << "bbb" << jMsg.dump() << std::endl;
	return jMsg.dump();
}

/*
making a json msg from this response
input: the response
outpt: the msg
*/
std::string JsonResponsePacketSerializer::serializeResponse(HighscoreResponse response)
{
	json jMsg;
	jMsg[HIGHSCORE_STATUS] = statusToString(response.status);
	jMsg[HIGHSCORE_HIGHSCORES] = response.highscores;
	return jMsg.dump();
}

/*
making a json msg from this response
input: the response
outpt: the msg
*/
std::string JsonResponsePacketSerializer::serializeResponse(JoinRoomResponse response)
{
	json jMsg;
	jMsg[JOIN_ROOM_STATUS] = statusToString(response.status);
	return jMsg.dump();
}

/*
making a json msg from this response
input: the response
outpt: the msg
*/
std::string JsonResponsePacketSerializer::serializeResponse(LogoutResponse response)
{
	json jMsg;
	jMsg[LOGOUT_STATUS] = statusToString(response.status);
	return jMsg.dump();
}

/*
making a json msg from this response
input: the response
outpt: the msg
*/
std::string JsonResponsePacketSerializer::serializeResponse(CloseRoomResponse response)
{
	json jMsg;
	jMsg[CLOSE_ROOM_STATUS] = statusToString(response.status);
	return jMsg.dump();
}

/*
making a json msg from this response
input: the response
outpt: the msg
*/
std::string JsonResponsePacketSerializer::serializeResponse(StartGameResponse response)
{
	json jMsg;
	jMsg[START_GAME_STATUS] = statusToString(response.status);
	return jMsg.dump();
}

/*
making a json msg from this response
input: the response
outpt: the msg
*/
std::string JsonResponsePacketSerializer::serializeResponse(GetRoomStateResponse response)
{
	json jMsg;
	jMsg[GET_ROOM_STATE_STATUS] = statusToString(response.status);
	jMsg[GET_ROOM_STATE_HAS_GAME_BUGUN] = response.hasGameBegun;
	jMsg[GET_ROOM_STATE_IS_ADMIN_IN_ROOM] = response.isAdminInRoom;
	jMsg[GET_ROOM_STATE_PLAYERS] = response.players;
	jMsg[GET_ROOM_STATE_QUESTION_COUNT] = response.questionCount;
	jMsg[GET_ROOM_STATE_QUESTION_TIMEOUT] = response.questionTimeout;
	return jMsg.dump();
}

/*
making a json msg from this response
input: the response
outpt: the msg
*/
std::string JsonResponsePacketSerializer::serializeResponse(LeaveRoomResponse response)
{
	json jMsg;
	jMsg[LEAVE_ROOM_STATUS] = statusToString(response.status);
	return jMsg.dump();
}
