#pragma once
#include "projectExceptions.h"
#include "Responses\Responses.h"
#include "externalFiles\json.hpp"
#include "RequestIdProtocols.h"
using json = nlohmann::json;

#include <string>

class JsonResponsePacketSerializer
{
public:
	//make formatted json messages depending on the resposne type:
	static std::string serializeResponse(ErrorResponse response);
	static std::string serializeResponse(LoginResponse response);
	static std::string serializeResponse(SignupResponse response);
	static std::string serializeResponse(CreateRoomResponse response);
	static std::string serializeResponse(GetPlayersInRoomResponse response);
	static std::string serializeResponse(GetRoomsResponse response);
	static std::string serializeResponse(HighscoreResponse response);
	static std::string serializeResponse(JoinRoomResponse response);
	static std::string serializeResponse(LogoutResponse response);
	static std::string serializeResponse(CloseRoomResponse response);
	static std::string serializeResponse(StartGameResponse response);
	static std::string serializeResponse(GetRoomStateResponse response);
	static std::string serializeResponse(LeaveRoomResponse response);
};

//turns int status to string status
static std::string statusToString(unsigned int status);