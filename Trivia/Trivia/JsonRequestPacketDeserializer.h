#pragma once
#include "projectExceptions.h"
#include "Requests\Requests.h"
#include "externalFiles\json.hpp"
using json = nlohmann::json;

#include <string>

class JsonRequestPacketDeserializer
{
public:
	static LoginRequest deserializeLoginRequest(const std::string msg);
	static SignupRequest deserializeSignupRequest(const std::string  msg);
	static CreateRoomRequest deserializeCreateRoomRequest(const std::string msg);
	static GetPlayersInRoomRequest deserializeGetPlayersInRoomRequest(const std::string msg);
	static JoinRoomRequest deserializeJoinRoomRequest(const std::string msg);
};