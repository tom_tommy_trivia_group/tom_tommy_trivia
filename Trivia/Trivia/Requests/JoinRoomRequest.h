#pragma once
#include "IRequest.h"

#define JOIN_ROOM_ID "roomid"

struct JoinRoomRequest : public IRequest
{
public:
	JoinRoomRequest(unsigned int roomId);//construct the new request
	JoinRoomRequest(const JoinRoomRequest& other);//copy constructor
	JoinRoomRequest();//Default constructor
	virtual ~JoinRoomRequest() {};//Default destructor

	unsigned int roomId;
};