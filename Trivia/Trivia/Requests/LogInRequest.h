#pragma once
#include "IRequest.h"
#include <string>

//fields:
#define LOG_IN_USERNAME "username"
#define LOG_IN_PASSWORD "password"

struct LoginRequest : public IRequest
{
public:
	LoginRequest(const std::string username, const std::string password);//construct z new request
	LoginRequest(const LoginRequest& other);//copy constructor
	LoginRequest();//Defult constructor
	virtual ~LoginRequest() {};//Defult desttructor

	std::string username;
	std::string password;
};