#include "GetPlayersInRoomRequest.h"

//constructor
GetPlayersInRoomRequest::GetPlayersInRoomRequest(unsigned int roomId)
{
	this->roomId = roomId;
}

//copy constructor
GetPlayersInRoomRequest::GetPlayersInRoomRequest(const GetPlayersInRoomRequest& other)
{
	this->roomId = other.roomId;
}

//default constructor
GetPlayersInRoomRequest::GetPlayersInRoomRequest() : roomId(0)
{
}