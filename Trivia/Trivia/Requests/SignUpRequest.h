#pragma once
#include "IRequest.h"
#include <string>

//fileds:
#define SIGN_UP_USERNAME "username"
#define SIGN_UP_PASSWORD "password"
#define SIGN_UP_EMAIL "email"


struct SignupRequest : public IRequest
{
public:
	SignupRequest(const std::string username, const std::string password, const std::string email);//construct z new request
	SignupRequest(const SignupRequest& other);//copy constructor
	SignupRequest();//Defult constructor
	virtual ~SignupRequest() {};//Defult desttructor

	std::string username;
	std::string password;
	std::string email;
};