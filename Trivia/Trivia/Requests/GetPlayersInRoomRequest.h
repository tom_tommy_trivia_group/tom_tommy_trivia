#pragma once
#include "IRequest.h"

#define GET_PLAYERS_IN_ROOM_ID "roomid"

struct GetPlayersInRoomRequest : public IRequest
{
public:
	GetPlayersInRoomRequest(unsigned int roomId);//construct the new request
	GetPlayersInRoomRequest(const GetPlayersInRoomRequest& other);//copy constructor
	GetPlayersInRoomRequest();//Default constructor
	virtual ~GetPlayersInRoomRequest() {};//Default destructor

	unsigned int roomId;
};