#include "JoinRoomRequest.h"

//constructor
JoinRoomRequest::JoinRoomRequest(unsigned int roomId)
{
	this->roomId = roomId;
}

//copy constructor
JoinRoomRequest::JoinRoomRequest(const JoinRoomRequest& other)
{
	this->roomId = other.roomId;
}

//default constructor
JoinRoomRequest::JoinRoomRequest() : roomId(0)
{
}