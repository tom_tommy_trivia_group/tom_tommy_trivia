#pragma once
#include "IRequest.h"
#include <string>

#define CREATE_ROOM_NAME "roomname"
#define CREATE_ROOM_MAX_USERS "maxusers"
#define CREATE_ROOM_QUESTION_COUNT "questioncount"
#define CREATE_ROOM_QUESTION_TIMEOUT "questiontimeout"

struct CreateRoomRequest : public IRequest
{
public:
	CreateRoomRequest(std::string roomName, unsigned int maxUsers, unsigned int questionCount, unsigned int questionTimeout);//construct the new request
	CreateRoomRequest(const CreateRoomRequest& other);//copy constructor
	CreateRoomRequest();//Default constructor
	virtual ~CreateRoomRequest() {};//Default destructor

	std::string roomName;
	unsigned int maxUsers;
	unsigned int questionCount;
	unsigned int questionTimeout;
};