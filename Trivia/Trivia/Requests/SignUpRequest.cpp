#include "SignUpRequest.h"

/*
construct the request
input: the field of the request
output: non
*/
SignupRequest::SignupRequest(const std::string username, const std::string password, const std::string email)
{
	this->username = username;
	this->password = password;
	this->email = email;
}

/*
construct the request (copy construction)
input: the other request
output: non
*/
SignupRequest::SignupRequest(const SignupRequest& other)
{
	this->username = other.username;
	this->password = other.password;
	this->email = other.email;
}

/*
defult construct the request
input: non
output: non
*/
SignupRequest::SignupRequest()
{
	this->username = "";
	this->password = "";
	this->email = "";
}