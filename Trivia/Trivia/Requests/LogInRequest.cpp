#include "LogInRequest.h"

#include "SignUpRequest.h"

/*
construct the request
input: the field of the request
output: non
*/
LoginRequest::LoginRequest(const std::string username, const std::string password)
{
	this->username = username;
	this->password = password;
}

/*
construct the request (copy construction)
input: the other request
output: non
*/
LoginRequest::LoginRequest(const LoginRequest& other)
{
	this->username = other.username;
	this->password = other.password;
}

/*
defult construct the request
input: non
output: non
*/
LoginRequest::LoginRequest()
{
	this->username = "";
	this->password = "";
}