#include "CreateRoomRequest.h"

//constructor
CreateRoomRequest::CreateRoomRequest(std::string roomName, unsigned int maxUsers, unsigned int questionCount, unsigned int questionTimeout)
{
	this->roomName = roomName;
	this->maxUsers = maxUsers;
	this->questionCount = questionCount;
	this->questionTimeout = questionTimeout;
}

//copy constructor
CreateRoomRequest::CreateRoomRequest(const CreateRoomRequest& other)
{
	this->roomName = other.roomName;
	this->maxUsers = other.maxUsers;
	this->questionCount = other.questionCount;
	this->questionTimeout = other.questionTimeout;
}

//default constructor
CreateRoomRequest::CreateRoomRequest() : roomName(""), maxUsers(0), questionCount(0), questionTimeout(0)
{
}