#pragma once
/*
this file include all the requests.
*/

#include "LogInRequest.h"
#include "SignUpRequest.h"
#include "CreateRoomRequest.h"
#include "GetPlayersInRoomRequest.h"
#include "JoinRoomRequest.h"