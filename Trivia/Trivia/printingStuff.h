#pragma once

//errors
//errors
#define SOCKET_CREATION_ERROR ("ERROR:	 failed to create communicator socket\n")
#define CLOSING_ERROR ("ERROR:	 failed to disconnect client while closing the server")
#define BINDING_ERROR ("ERROR:	 communicator failed to bind\n")
#define LISTENING_ERROR ("ERROR:	 communicator failed to listen\n")
#define CLIENT_SOCKET_INIT_ERROR ("ERROR:	failed to initialize client socket");



#define LISTENING_MSG ("NOTE:	communicator is listen to clients on \n")
#define WAITING_MSG ("NOTE:	Waiting for client connection request\n")