#pragma once
//#include <stdio>
#include <string>

/*
Goal of Buffer:
it needs to behave like const string, but be open to further changes
*/
class Buffer
{
public:
	Buffer(std::string value);
	virtual ~Buffer() {};
	operator  std::string() const;
private:
	std::string _value;
};
