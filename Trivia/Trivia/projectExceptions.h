#pragma once
#include <exception>
#include <string>
enum problemKind { UNVALID_MSG };

class Problem : public std::exception
{
public:
	Problem(problemKind king, std::string description); //construct a new Problem
	virtual ~Problem() {};//destruct a new Problem
	virtual const char* what() const;//describe the Problem
	problemKind kind();//kind of Problem
private:
	problemKind _kind;
	std::string _description;
};