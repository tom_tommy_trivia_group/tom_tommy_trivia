#pragma once
#include "IResponse.h"
#include "..\Managers\Room.h"
#include <vector>


#define DEFAULT_GET_ROOMS_STATSUS 0

//fileds:
#define GET_ROOMS_STATUS "status"
#define GET_ROOMS_ID "roomsid"
#define GET_ROOMS_NAMES "roomsnames"
#define GET_ROOMS_CURR_USERS "roomscurrusers"
#define GET_ROOMS_MAX_USERS "roomsmaxusers"

struct GetRoomsResponse : public Iresponse
{
public:
	GetRoomsResponse(const unsigned int currStatus, std::vector<std::shared_ptr<Room>> rooms);//construct the new response
	GetRoomsResponse();//Defult constructor
	virtual ~GetRoomsResponse();//Defult destructor

	unsigned int status;
	std::vector<std::shared_ptr<Room>> rooms;
};