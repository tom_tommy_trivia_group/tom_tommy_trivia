#include "GetRoomStateResponse.h"

/*
create a new GetRoomStateResponse when the status is known.
input - the err description
*/
GetRoomStateResponse::GetRoomStateResponse(const unsigned int currStatus, bool hasBegun, std::vector<std::string> players, unsigned int qCount, unsigned int qTimeout) :
	status(currStatus), hasGameBegun(hasBegun), players(players), questionCount(qCount), questionTimeout(qTimeout)
{
}

/*
default constructor
*/
GetRoomStateResponse::GetRoomStateResponse() :
	status(DEFAULT_GET_ROOM_STATE_STATUS), hasGameBegun(false), questionCount(0), questionTimeout(0)
{
}