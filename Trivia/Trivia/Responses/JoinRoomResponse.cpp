#include "JoinRoomResponse.h"

//constructor when the error status is known
JoinRoomResponse::JoinRoomResponse(unsigned int status)
{
	this->status = status;
}

//default constructor
JoinRoomResponse::JoinRoomResponse() : status(DEFAULT_JOIN_ROOM_STASUS)
{
}