#include "HighscoreResponse.h"


/*
create a new highscore response when the status is known.
input - the err description
*/
HighscoreResponse::HighscoreResponse(const unsigned int currStatus, std::map<std::string, int> highscores)
{
	this->status = currStatus;
	this->highscores = highscores;
}

/*
init the highscore response with default status
*/
HighscoreResponse::HighscoreResponse()
{
	this->status = DEFAULT_HIGH_SCORE_STASUS;
}