#pragma once
#include "IResponse.h"


#define DEFAULT_CREATE_ROOM_STASUS 0

//fileds:
#define CREATE_ROOM_STATUS "status"

struct CreateRoomResponse : public Iresponse
{
public:
	CreateRoomResponse(const unsigned int currStatus);//construct a new response
	CreateRoomResponse();//Defult constructor
	virtual ~CreateRoomResponse() {};//Defult desttructor

	unsigned int status;
};