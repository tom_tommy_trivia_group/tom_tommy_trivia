#pragma once
#include "IResponse.h"


#define DEFAULT_CLOSE_ROOM_STATUS 0

//fields:
#define CLOSE_ROOM_STATUS "status"

struct CloseRoomResponse : public Iresponse
{
public:
	CloseRoomResponse(const unsigned int currStatus);//construct a new response
	CloseRoomResponse();//Defult constructor
	virtual ~CloseRoomResponse() {};//Defult desttructor

	unsigned int status;
};