#pragma once
#include "IResponse.h"


#define DEFAULT_LOGIN_STASUS 0

//fileds:
#define LOGIN_STATUS "status"

struct LoginResponse : public Iresponse
{
public:
	LoginResponse(const unsigned int currStatus);//construct z new response
	LoginResponse();//Defult constructor
	virtual ~LoginResponse() {};//Defult destructor

	unsigned int status;
};