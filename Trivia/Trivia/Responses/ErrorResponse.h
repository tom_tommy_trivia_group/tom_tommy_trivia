#pragma once
#include "IResponse.h"
#include <string>

//fileds:
#define ERROR_MESSEGE "message"

#define DEFAULT_ERROR "unknown Error"

struct ErrorResponse : public Iresponse
{
public:
	ErrorResponse(const std::string desription);//construct z new response
	ErrorResponse();//Defult constructor
	virtual ~ErrorResponse() {};//Defult desttructor

	std::string message;
};