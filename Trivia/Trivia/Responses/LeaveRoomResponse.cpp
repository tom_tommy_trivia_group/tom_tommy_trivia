#include "LeaveRoomResponse.h"

/*
create a new LeaveRoomResponse when the status is known.
input - the err description
*/
LeaveRoomResponse::LeaveRoomResponse(const unsigned int currStatus) :
	status(currStatus)
{
}

/*
default constructor
*/
LeaveRoomResponse::LeaveRoomResponse() :
	status(DEFAULT_LEAVE_ROOM_STATUS)
{
}