#pragma once
#include "IResponse.h"
#include <vector>


#define DEFAULT_JOIN_ROOM_STASUS 0

//fields:
#define JOIN_ROOM_STATUS "status"

struct JoinRoomResponse : public Iresponse
{
public:
	JoinRoomResponse(const unsigned int currStatus);//construct the new response
	JoinRoomResponse();//Defult constructor
	virtual ~JoinRoomResponse() {};//Default destructor

	unsigned int status;
};