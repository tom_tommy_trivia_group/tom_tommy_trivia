#pragma once
#include "IResponse.h"
#include <string>
#include <vector>

#define GET_PLAYERS_IN_ROOM_KEY "players"

struct GetPlayersInRoomResponse : public Iresponse
{
public:
	GetPlayersInRoomResponse(std::vector<std::string> rooms);//construct the new response
	GetPlayersInRoomResponse();//Defult constructor
	virtual ~GetPlayersInRoomResponse() {};//Default destructor

	std::vector<std::string> players;
};