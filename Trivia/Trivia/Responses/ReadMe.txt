This directory contain all the kind of the responses from user.

For including all the kinds of responses include "Responses.h"
All the clases are based on IResponse

List of all Reqwests added by vertion:
	Vertion 1.0.0:
		ErrorResponse
		LogInResponse
		SignUpResponse
	Vertion 2.0.0:
		LogoutResponse
		GetRoomsResponse
		GetPlayersInRoomResponse
		HighScoreResponse
		JoinRoomResponse
		CreateRoomResponse
	Vertion 3.0.0:
		CloseRoomResponse
		StartGameResponse
		GetRoomStateResponse
		LeaveRoomResponse
		AllPlayersReadyResponse
	Vertion 4.0.0:
		///TBD