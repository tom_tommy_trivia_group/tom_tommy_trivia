#include "LogoutResponse.h"


/*
create a new LogoutResponse when the status is known.
input - the err description
*/
LogoutResponse::LogoutResponse(const unsigned int currStatus)
{
	this->status = currStatus;
}

/*
init the logout response with default status
*/
LogoutResponse::LogoutResponse()
{
	this->status = DEFAULT_LOGOUT_STASUS;
}