#include "SignUpResponse.h"


/*
create a new SignupResponse when the status is known.
input - the description of the error
*/
SignupResponse::SignupResponse(const unsigned int currStatus)
{
	this->status = currStatus;
}

/*
init the Signup Response with default stustus
*/
SignupResponse::SignupResponse()
{
	this->status = DEFAULT_SIGNUP_STASUS;
}