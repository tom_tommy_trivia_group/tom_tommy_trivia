#include "GetPlayersInRoomResponse.h"

/*
create a new Get Players In Room Response when the status is known.
input - the players in the room
*/
GetPlayersInRoomResponse::GetPlayersInRoomResponse(std::vector <std::string> players)
{
	this->players = players;
}

/*
init the Get Players In Room Response response with default status
*/
GetPlayersInRoomResponse::GetPlayersInRoomResponse()
{
}