#include "GetRoomsResponse.h"


/*
create a new Get Rooms Response when the status is known.
input - the err description
*/
GetRoomsResponse::GetRoomsResponse(const unsigned int currStatus, std::vector<std::shared_ptr<Room>> rooms)
{
	this->status = currStatus;
	this->rooms = rooms;
}

/*
init the Get Rooms Response response with default status
*/
GetRoomsResponse::GetRoomsResponse()
{
	this->status = DEFAULT_GET_ROOMS_STATSUS;
}

/*
destructor
*/
GetRoomsResponse::~GetRoomsResponse()
{
	for (auto it = rooms.begin(); it != rooms.end(); ++it)
	{
		it->reset();
	}
}