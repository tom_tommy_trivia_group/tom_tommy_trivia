#pragma once
/*
this file include all the requests.
*/

#include "ErrorResponse.h"
#include "LogInResponse.h"
#include "SignUpResponse.h"
#include "CreateRoomResponse.h"
#include "GetPlayersInRoomResponse.h"
#include "GetRoomsResponse.h"
#include "HighscoreResponse.h"
#include "JoinRoomResponse.h"
#include "LogoutResponse.h"
#include "CloseRoomResponse.h"
#include "StartGameResponse.h"
#include "GetRoomStateResponse.h"
#include "LeaveRoomResponse.h"