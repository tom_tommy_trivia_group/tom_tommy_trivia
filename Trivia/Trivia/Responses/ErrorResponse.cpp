#include "ErrorResponse.h"
/*
create a new errorResponse when the error is known.
input - the description of the error
*/
ErrorResponse::ErrorResponse(const std::string description)
{
	this->message = description;
}

/*
init the error with default eror
*/
ErrorResponse::ErrorResponse()
{
	this->message = DEFAULT_ERROR;
}
