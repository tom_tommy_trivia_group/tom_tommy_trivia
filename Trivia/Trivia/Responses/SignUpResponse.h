#pragma once
#include "IResponse.h"


#define DEFAULT_SIGNUP_STASUS 0

//fileds:
#define SIGNUP_STATUS "status"

struct SignupResponse : public Iresponse
{
public:
	SignupResponse(const unsigned int currStatus);//construct a new response
	SignupResponse();//Defult constructor
	virtual ~SignupResponse() {};//Defult desttructor

	unsigned int status;
};