#pragma once
#include "IResponse.h"
#include <map>


#define DEFAULT_HIGH_SCORE_STASUS 0

//fields:
#define HIGHSCORE_STATUS "status"
#define HIGHSCORE_HIGHSCORES "highscores"

struct HighscoreResponse : public Iresponse
{
public:
	HighscoreResponse(const unsigned int currStatus, std::map<std::string, int> highScores);//construct the new response
	HighscoreResponse();//Defult constructor
	virtual ~HighscoreResponse() {};//Defult destructor

	unsigned int status;
	std::map<std::string, int> highscores;
};