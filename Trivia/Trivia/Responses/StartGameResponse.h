#pragma once
#include "IResponse.h"


#define DEFAULT_START_GAME_STATUS 0

//fields:
#define START_GAME_STATUS "status"

struct StartGameResponse : public Iresponse
{
public:
	StartGameResponse(const unsigned int currStatus);//construct a new response
	StartGameResponse();//Defult constructor
	virtual ~StartGameResponse() {};//Defult desttructor

	unsigned int status;
};