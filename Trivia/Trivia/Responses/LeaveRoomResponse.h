#pragma once
#include "IResponse.h"


#define DEFAULT_LEAVE_ROOM_STATUS 0

//fields:
#define LEAVE_ROOM_STATUS "status"

struct LeaveRoomResponse : public Iresponse
{
public:
	LeaveRoomResponse(const unsigned int currStatus);//construct a new response
	LeaveRoomResponse();//Defult constructor
	virtual ~LeaveRoomResponse() {};//Defult desttructor

	unsigned int status;
};