#include "CreateRoomResponse.h"

/*
create a new CreateRoomResponse when the status is known.
input - the err description
*/
CreateRoomResponse::CreateRoomResponse(const unsigned int currStatus) :
	status(currStatus)
{
}

/*
init the CreateRoomResponse response with default status
*/
CreateRoomResponse::CreateRoomResponse() :
	status(DEFAULT_CREATE_ROOM_STASUS)
{
}