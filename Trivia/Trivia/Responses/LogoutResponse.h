#pragma once
#include "IResponse.h"


#define DEFAULT_LOGOUT_STASUS 0

//fileds:
#define LOGOUT_STATUS "status"

struct LogoutResponse : public Iresponse
{
public:
	LogoutResponse(const unsigned int currStatus);//construct the new response
	LogoutResponse();//Defult constructor
	virtual ~LogoutResponse() {};//Defult desttructor

	unsigned int status;
};