#include "StartGameResponse.h"

/*
create a new StartGameResponse when the status is known.
input - the err description
*/
StartGameResponse::StartGameResponse(const unsigned int currStatus) :
	status(currStatus)
{
}

/*
init the StartGameResponse response with default status
*/
StartGameResponse::StartGameResponse() :
	status(DEFAULT_START_GAME_STATUS)
{
}