#pragma once
#include "IResponse.h"
#include <string>
#include <vector>


#define DEFAULT_GET_ROOM_STATE_STATUS 0

//fields:
#define GET_ROOM_STATE_STATUS "status"
#define GET_ROOM_STATE_HAS_GAME_BUGUN "hasgamebegun"
#define GET_ROOM_STATE_IS_ADMIN_IN_ROOM "isadmininroom"
#define GET_ROOM_STATE_PLAYERS "players"
#define GET_ROOM_STATE_QUESTION_COUNT "questioncount"
#define GET_ROOM_STATE_QUESTION_TIMEOUT "questiontimeout"

struct GetRoomStateResponse : public Iresponse
{
public:
	GetRoomStateResponse(const unsigned int currStatus, bool hasBegun, std::vector<std::string> players, unsigned int qCount, unsigned int qTimeout);//construct a new response
	GetRoomStateResponse();//Defult constructor
	virtual ~GetRoomStateResponse() {};//Defult desttructor

	unsigned int status;
	bool hasGameBegun;
	bool isAdminInRoom;
	std::vector<std::string> players;
	unsigned int questionCount;
	unsigned int questionTimeout;
};