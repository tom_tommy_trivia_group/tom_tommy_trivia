#include "CloseRoomResponse.h"

/*
create a new CloseRoomResponse when the status is known.
input - the err description
*/
CloseRoomResponse::CloseRoomResponse(const unsigned int currStatus) :
	status(currStatus)
{
}

/*
init the CloseRoomResponse response with default status
*/
CloseRoomResponse::CloseRoomResponse() :
	status(DEFAULT_CLOSE_ROOM_STATUS)
{
}