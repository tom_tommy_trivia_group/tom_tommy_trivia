#include "RequestHandleFactory.h"
#include "Handlers\LoginRequestHandler.h"
#include "Handlers\MenuRequestHandler.h"
#include "Handlers\RoomAdminRequestHandler.h"
#include "Handlers\RoomMemberRequestHandler.h"

//constructor
RequestHandlerFactory::RequestHandlerFactory(std::shared_ptr<SQLiteDatabase> db) : m_loginManager(std::shared_ptr<LoginManager>(new LoginManager(db))), m_roomManager(std::shared_ptr<RoomManager>(new RoomManager)), m_highscoreTable(std::shared_ptr<HighscoreTable>(new HighscoreTable(db)))
{
}

//destructor
RequestHandlerFactory::~RequestHandlerFactory()
{
	m_loginManager.reset();
	m_roomManager.reset();
	m_highscoreTable.reset();
}

//getters:
std::shared_ptr<LoginManager> RequestHandlerFactory::getLoginManager()
{
	return this->m_loginManager;
}

std::shared_ptr<RoomManager> RequestHandlerFactory::getRoomManager()
{
	return this->m_roomManager;
}

std::shared_ptr<HighscoreTable> RequestHandlerFactory::getHighscoreTable()
{
	return this->m_highscoreTable;
}

//equvilance operator
RequestHandlerFactory& RequestHandlerFactory::operator= (RequestHandlerFactory& other)
{
	this->m_loginManager = other.getLoginManager();

	return *(this);
}

//the function creates a new login request hander and returns it
LoginRequestHandler* RequestHandlerFactory::createLoginRequestHandler()
{
	//create the new handler with this factory and it's login manager
	LoginRequestHandler* newHandler = new LoginRequestHandler(std::shared_ptr<LoginManager>(this->m_loginManager), this);
	return newHandler;
}

//the function create a new menu request handler for the given logged user
MenuRequestHandler* RequestHandlerFactory::createMenuRequestHandler(std::shared_ptr<LoggedUser> user)
{
	/*
	create a new handler with the given user, this factory and it's room manager and it's highscore table
	*/
	MenuRequestHandler* newHandler = new MenuRequestHandler(std::shared_ptr<LoggedUser>(user), std::shared_ptr<RoomManager>(this->m_roomManager), std::shared_ptr<HighscoreTable>(this->m_highscoreTable), this);

	return newHandler;
}

//the function create a new room admin request handler for the admin of the room
RoomAdminRequestHandler* RequestHandlerFactory::createRoomAdminRequestHandler(std::shared_ptr<LoggedUser> user, std::shared_ptr<Room> room)
{
	/*
	create a new handler with the given user, this factory and it's room manager and it's highscore table
	*/
	RoomAdminRequestHandler* newHandler = new RoomAdminRequestHandler(std::shared_ptr<Room>(room), std::shared_ptr<LoggedUser>(user), std::shared_ptr<RoomManager>(this->m_roomManager), this);

	return newHandler;
}

//the function create a new member request handler for a player of the room
RoomMemberRequestHandler* RequestHandlerFactory::createRoomMemberRequestHandler(std::shared_ptr<LoggedUser> user, std::shared_ptr<Room> room)
{
	/*
	create a new handler with the given user, this factory and it's room manager and it's highscore table
	*/
	RoomMemberRequestHandler* newHandler = new RoomMemberRequestHandler(std::shared_ptr<Room>(room), std::shared_ptr<LoggedUser>(user), std::shared_ptr<RoomManager>(this->m_roomManager), this);

	return newHandler;
}