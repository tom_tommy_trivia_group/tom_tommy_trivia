#include "JsonRequestPacketDeserializer.h"
// just a sentense that would repeat a lot of times in this file. use it when you want to set  jsonSrc[key] in to some place and if you can not, add key to a string of errorPlace.
// note - it will put a red line under it, it is ok.

//the function will try to collect the value from the json source, and will indicate if the collection went wrong (by errIndicator)
//using typename allows T to be a class or a primitive
template<typename T>
void TRY_COLLECT_JSON(T& target, const json& jsonSrc, std::string key, std::string& errIndicator)
{
	try
	{
		target = jsonSrc[key];
	}
	catch (...)
	{
		errIndicator = key;
	}
}
/*#define TRY_COLLECT_JSON(place, jsonSrc, key, errorPlace) try\
															{\
																(place) = ((jsonSrc)[key]);\
															}\
														catch (...) \
															{\
																((errorPlace) += " ") += (key);\
															}*/

/*
makes a request from the json test
input: the text
output:the request
*/
LoginRequest JsonRequestPacketDeserializer::deserializeLoginRequest(const std::string msg)
{
	json jMsg;

	try 
	{
		jMsg = json::parse(msg);
	}
	catch (...) //the msg is invalid
	{
		throw Problem(UNVALID_MSG, "JsonRequestPacketDeserializer::deserializeLoginRequest got a problem - the msg is not a json one");
	}

	LoginRequest result;
	std::string filedMissing = "";
	
	//adding info to json
	TRY_COLLECT_JSON(result.username, jMsg, LOG_IN_USERNAME, filedMissing);
	TRY_COLLECT_JSON(result.password, jMsg, LOG_IN_PASSWORD, filedMissing);
	

	if ("" != filedMissing)
	{
		throw Problem(UNVALID_MSG, "JsonRequestPacketDeserializer::deserializeLoginRequest got a problem - this values are missing or in the wring type: " + filedMissing);
	}

	return result;
}

/*
makes a request from the json test
input: the text
output:the request
*/
SignupRequest JsonRequestPacketDeserializer::deserializeSignupRequest(const std::string msg)
{
	json jMsg;

	try
	{
		jMsg = json::parse(msg);
	}
	catch (...) //the msg is invalid
	{
		throw Problem(UNVALID_MSG, "JsonRequestPacketDeserializer::deserializeSignupRequest got a problem - the msg is not a json one");
	}

	SignupRequest result;
	std::string filedMissing = "";
	//adding info to json
	TRY_COLLECT_JSON(result.username, jMsg, SIGN_UP_USERNAME, filedMissing);
	TRY_COLLECT_JSON(result.email, jMsg, SIGN_UP_EMAIL, filedMissing);
	TRY_COLLECT_JSON(result.password, jMsg, SIGN_UP_PASSWORD, filedMissing);

	if ("" != filedMissing)
	{
		throw Problem(UNVALID_MSG, "JsonRequestPacketDeserializer::deserializeSignupRequest got a problem - this values are missing or in the wring type: " + filedMissing);
	}

	return result;
}

/*
makes a request from the json test
input: the text
output:the request
*/
CreateRoomRequest JsonRequestPacketDeserializer::deserializeCreateRoomRequest(const std::string msg)
{
	json jMsg;

	// these will be converted to strings after the json collection will be done
	std::string maxUsersStr, questionCountStr, questionTimeoutStr;

	try
	{
		jMsg = json::parse(msg);
	}
	catch (...) //the msg is invalid
	{
		throw Problem(UNVALID_MSG, "JsonRequestPacketDeserializer::deserializeCreateRoomRequest got a problem - the msg is not a json one");
	}

	CreateRoomRequest result;
	std::string filedMissing = "";
	//adding info to json
	TRY_COLLECT_JSON(result.roomName, jMsg, CREATE_ROOM_NAME, filedMissing);
	TRY_COLLECT_JSON(result.maxUsers, jMsg, CREATE_ROOM_MAX_USERS, filedMissing);
	TRY_COLLECT_JSON(result.questionCount, jMsg, CREATE_ROOM_QUESTION_COUNT, filedMissing);
	TRY_COLLECT_JSON(result.questionTimeout, jMsg, CREATE_ROOM_QUESTION_TIMEOUT, filedMissing);

	if ("" != filedMissing)
	{
		throw Problem(UNVALID_MSG, "JsonRequestPacketDeserializer::deserializeSignupRequest got a problem - this values are missing or in the wring type: " + filedMissing);
	}

	return result;
}

/*
makes a request from the json test
input: the text
output:the request
*/
GetPlayersInRoomRequest JsonRequestPacketDeserializer::deserializeGetPlayersInRoomRequest(const std::string msg)
{
	json jMsg;

	int roomId;

	try
	{
		jMsg = json::parse(msg);
	}
	catch (...) //the msg is invalid
	{
		throw Problem(UNVALID_MSG, "JsonRequestPacketDeserializer::deserializeGetPlayersInRoomRequest got a problem - the msg is not a json one");
	}

	GetPlayersInRoomRequest result;
	std::string filedMissing = "";
	//adding info to json
	TRY_COLLECT_JSON(roomId, jMsg, GET_PLAYERS_IN_ROOM_ID, filedMissing);

	if ("" != filedMissing)
	{
		throw Problem(UNVALID_MSG, "JsonRequestPacketDeserializer::deserializeGetPlayersInRoomRequest got a problem - this values are missing or in the wring type: " + filedMissing);
	}

	result.roomId = roomId;

	return result;
}

/*
makes a request from the json test
input: the text
output:the request
*/
JoinRoomRequest JsonRequestPacketDeserializer::deserializeJoinRoomRequest(const std::string msg)
{
	json jMsg;

	int roomId;

	try
	{
		jMsg = json::parse(msg);
	}
	catch (...) //the msg is invalid
	{
		throw Problem(UNVALID_MSG, "JsonRequestPacketDeserializer::deserializeJoinRoomRequest got a problem - the msg is not a json one");
	}

	JoinRoomRequest result;
	std::string filedMissing = "";
	//adding info to json
	TRY_COLLECT_JSON(roomId, jMsg, JOIN_ROOM_ID, filedMissing);

	if ("" != filedMissing)
	{
		throw Problem(UNVALID_MSG, "JsonRequestPacketDeserializer::deserializeJoinRoomRequest got a problem - this values are missing or in the wring type: " + filedMissing);
	}

	result.roomId = roomId;

	return result;
}