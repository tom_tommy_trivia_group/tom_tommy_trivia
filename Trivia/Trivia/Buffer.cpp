#include "Buffer.h"

/*
construct a new Buffer
input: string to be thiisbuffer value
output: non
*/
Buffer::Buffer(std::string value)
{
	this->_value = value;
}

/*
convert the buffer to const string
input: non
output: the const string it converted in to.
*/
Buffer::operator  std::string() const
{
	return this->_value;
}
