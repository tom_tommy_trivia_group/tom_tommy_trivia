#pragma once
#include "Communicator.h"

class Server
{
private:
	std::shared_ptr<Communicator> m_communicator;//this server's communicator
	std::shared_ptr<SQLiteDatabase> m_database;//this server's database
	std::shared_ptr<RequestHandlerFactory> m_factory;//this server's handler factory
public:
	Server(std::shared_ptr<Communicator> communicator, std::shared_ptr<SQLiteDatabase> db, std::shared_ptr<RequestHandlerFactory> factory);//constrctor
	~Server();//destructor
	void serve();//server function
};