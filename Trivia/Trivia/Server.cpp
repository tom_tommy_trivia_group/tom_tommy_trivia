#include "Server.h"

//constructor:
Server::Server(std::shared_ptr<Communicator> communicator, std::shared_ptr<SQLiteDatabase> db, std::shared_ptr<RequestHandlerFactory> factory) : m_database(db), m_factory(factory), m_communicator(communicator)
{
}

//destructor:
Server::~Server()
{
	this->m_database.reset();
	this->m_factory.reset();
	this->m_communicator.reset();
}

//the function activates the serving process
void Server::serve()
{
	this->m_communicator->bindAndListen();
}