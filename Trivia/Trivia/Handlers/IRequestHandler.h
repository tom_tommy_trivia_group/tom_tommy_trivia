#pragma once
#include "Request.h"
#include "RequestResult.h"

class IRequestHandler
{
public:
	IRequestHandler();//constructor
	virtual ~IRequestHandler();//destructor
	virtual bool isRequestRelevant(const Request &req) const = 0;//function checks if the request is relevnt
	virtual RequestResult* handleRequest(Request req) = 0;//the function handles the user's request
};