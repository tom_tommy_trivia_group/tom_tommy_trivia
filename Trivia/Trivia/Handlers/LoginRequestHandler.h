#pragma once
#include "IrequestHandler.h"
#include "..\JsonRequestPacketDeserializer.h"
#include "..\JsonResponsePacketSerializer.h"
#include "..\RequestHandleFactory.h"
#include "..\RequestIdProtocols.h"

class LoginRequestHandler : public IRequestHandler
{
public:
	LoginRequestHandler(std::shared_ptr<LoginManager> manager, RequestHandlerFactory *factory); //constructor

	virtual ~LoginRequestHandler();//destructor

	virtual bool isRequestRelevant(const Request &req) const;//the function checks if the request is relevant
	virtual RequestResult* handleRequest(Request req);//the function does what the user's state requires
private:
	std::shared_ptr<LoginManager> m_loginManager;
	RequestHandlerFactory *m_handlerFactory;

	RequestResult* login(Request req);//the function tries to log the user in
	RequestResult* signup(Request req);//the function signes the user up
	RequestResult* signout();//the removes the client from the state machine
};