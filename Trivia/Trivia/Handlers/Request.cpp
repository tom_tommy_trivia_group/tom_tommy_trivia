#include "Request.h"

Request::Request(int id, std::chrono::system_clock::time_point time, std::string b) : requestId(id), receivalTime(time), buffer(b)
{
}

Request::Request()
{
	this->requestId = -1;
	this->receivalTime = std::chrono::system_clock::now();
	this->buffer = "";
}