#pragma once

#include "IrequestHandler.h"
#include "..\JsonRequestPacketDeserializer.h"
#include "..\JsonResponsePacketSerializer.h"
#include "..\RequestHandleFactory.h"

class RoomMemberRequestHandler : public IRequestHandler
{
public:
	RoomMemberRequestHandler(std::shared_ptr<Room> room, std::shared_ptr<LoggedUser> user, std::shared_ptr<RoomManager> roomManager, RequestHandlerFactory* factory);//constructor
	virtual ~RoomMemberRequestHandler();//destructor

	virtual bool isRequestRelevant(const Request &req) const;//the function checks if the request is relevant
	virtual RequestResult* handleRequest(Request req);//the function does what the user's state requires

private:
	//fields:
	std::shared_ptr<Room> m_room;
	std::shared_ptr<LoggedUser> m_user;
	std::shared_ptr<RoomManager> m_roomManager;
	RequestHandlerFactory* m_factory;

	//private functions:
	RequestResult* leaveRoom();//the function caters to user's leaveRoom request
	RequestResult* getRoomState();//the function caters to user's startRoom request
	RequestResult* signout();//the admin can signout midgame -this handler caters to his request
};