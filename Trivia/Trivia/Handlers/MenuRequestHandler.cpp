#include "MenuRequestHandler.h"

//constructor
MenuRequestHandler::MenuRequestHandler(std::shared_ptr<LoggedUser> user, std::shared_ptr<RoomManager> roomManager, std::shared_ptr<HighscoreTable> highscoreTable, RequestHandlerFactory* factory) : m_user(user), m_roomManager(roomManager), m_highscoreTable(highscoreTable), m_factory(factory)
{
}

//destructor
MenuRequestHandler::~MenuRequestHandler()
{
	this->m_user.reset();
	this->m_highscoreTable.reset();
	this->m_roomManager.reset();
}

bool MenuRequestHandler::isRequestRelevant(const Request &req) const
{
	bool isRelevant = false;

	if (req.requestId >= MIN_MENU_REQ_HANDLER_ID && req.requestId <= MAX_REQ_ID)
	{
		isRelevant = true;
	}

	return isRelevant;
}

RequestResult* MenuRequestHandler::handleRequest(Request req)
{
	RequestResult* res = nullptr;
	
	switch (req.requestId)
	{
	case GET_ROOMS:
		res = this->getRooms();//return the rooms and the new state
		break;

	case GET_PLAYERS_IN_ROOM:
		res = this->getPlayersInRoom(req);//return the players in the room and the new state
		break;

	case GET_HIGHSCORES:
		res = this->getHighscores();//return the highscores and the new state
		break;

	case JOIN_ROOM:
		res = this->joinRoom(req);//return result of the joinRoom mehtod and the new state
		break;

	case CREATE_ROOM:
		res = this->createRoom(req);//return result of the room creation mehtod and the new state
		break;

	case SIGNOUT:
		res = this->signout();//signout the user. no need for a result because the user is no longer connected
		break;

	case GET_ROOM_STATE:
		res = this->getRooms();
		break;

	default:
		throw Problem(UNVALID_MSG, "MenuRequestHandler - the request Id is not in one of the current options");
		break;
	}
	
	return res;
}

//function gets returs a list of all the rooms that are currently in the sever as a formatted list
RequestResult* MenuRequestHandler::getRooms()
{
	unsigned int status = STATUS_OK;
	JsonResponsePacketSerializer serializer;
	IRequestHandler* newHandler = nullptr;
	GetRoomsResponse res;
	res.rooms = this->m_roomManager->getRooms();

	res.status = status;

	return new RequestResult(serializer.serializeResponse(res), newHandler);
}

RequestResult* MenuRequestHandler::getPlayersInRoom(Request req)
{
	JsonResponsePacketSerializer serializer;
	JsonRequestPacketDeserializer des;
	IRequestHandler* newHandler = nullptr;
	GetPlayersInRoomRequest getPlayersReq(des.deserializeGetPlayersInRoomRequest(req.buffer));
	GetPlayersInRoomResponse res;
	auto room = this->m_roomManager->getRoomById(getPlayersReq.roomId);

	if (room != nullptr)
	{//if the room exists
		try
		{
			auto users = room->getAllUsers();

			for (auto iter = users.begin(); iter != users.end(); ++iter)
			{//moving over all the users
				res.players.push_back((*iter)->getUsername());//getting the usernames
			}
		}
		catch (...)
		{
			std::cout << "Something went wrong with the getPlayersInRoom() method" << std::endl;
		}
	}

	else
	{
		std::cout << "Get players in room - room not found" << std::endl;
	}


	return new RequestResult(serializer.serializeResponse(res), newHandler);
}

//returns a formatted string of the high scores
RequestResult* MenuRequestHandler::getHighscores()
{
	unsigned int status = STATUS_OK;
	JsonResponsePacketSerializer serializer;
	IRequestHandler* newHandler = nullptr;
	HighscoreResponse res;

	try
	{
		res.highscores = this->m_highscoreTable->getHighscores();
	}
	catch (...)
	{
		std::cout << "Something went wrong with the getHighScores() method" << std::endl;
		status = STATUS_UNKNOWN_ERROR;
	}

	res.status = status;

	return new RequestResult(serializer.serializeResponse(res), newHandler);
}

//returns if the user managed to join the room
RequestResult* MenuRequestHandler::joinRoom(Request req)
{
	unsigned int status = STATUS_OK;
	bool roomFound = false;
	JsonResponsePacketSerializer serializer;
	JsonRequestPacketDeserializer des;
	IRequestHandler* newHandler = nullptr;
	JoinRoomRequest desirializedReq(des.deserializeJoinRoomRequest(req.buffer));
	JoinRoomResponse res;

	auto room = this->m_roomManager->getRoomById(desirializedReq.roomId);

	try
	{
		if (room != nullptr)
		{
			if (!room->isRoomFull())
			{
				room->addUser(this->m_user);
			}
			
			else
			{
				status = JOIN_ROOM_ROOM_IS_FULL;
			}
		}

		else
		{
			status = JOIN_ROOM_ROOM_NOT_FOUND;
		}
	}
	catch (...)
	{
		std::cout << "Something went wrong with the joinRoom() method" << std::endl;
		status = STATUS_UNKNOWN_ERROR;
	}

	if (status == STATUS_OK)
	{
		//create a new handler that serves this handler's user and has the room that the user joined into
		newHandler = (IRequestHandler*)(this->m_factory->createRoomMemberRequestHandler(this->m_user, room));

	}
	
	res.status = status;

	return new RequestResult(serializer.serializeResponse(res), newHandler);
}

//returns if the room creation process was successful
RequestResult* MenuRequestHandler::createRoom(Request req)
{
	unsigned int status = STATUS_OK;
	JsonResponsePacketSerializer serializer;
	JsonRequestPacketDeserializer des;
	IRequestHandler* newHandler = nullptr;
	CreateRoomRequest desirializedReq(des.deserializeCreateRoomRequest(req.buffer));
	CreateRoomResponse res;
	std::shared_ptr<Room> room;

	try
	{
		//try to create the room, convert the result into unsigned int
		status = (this->m_roomManager->createRoom(this->m_user, desirializedReq.roomName, desirializedReq.maxUsers, desirializedReq.questionCount, desirializedReq.questionTimeout));
		
	}
	catch (...)
	{
		std::cout << "Something went wrong with the joinRoom() method" << std::endl;
		status = STATUS_UNKNOWN_ERROR;
	}

	res.status = status;

	if (status == STATUS_OK)
	{
		//create a new Admin handler that caters to this handlers users, and is connected to the new room that the admin had created
		newHandler = (IRequestHandler*)(this->m_factory->createRoomAdminRequestHandler(this->m_user, this->m_roomManager->getRoomByName(desirializedReq.roomName)));
		//(we just created the room so we can assume it is at the back of the room list)

	}

	return new RequestResult(serializer.serializeResponse(res), newHandler);
}

//try to remove the user from the signed list and returns the result of that removal
RequestResult* MenuRequestHandler::signout()
{
	unsigned int status = STATUS_OK;
	JsonResponsePacketSerializer serializer;
	IRequestHandler* newHandler = nullptr;
	LogoutResponse res;
	auto rooms = this->m_roomManager->getRooms();

	try
	{
		for (auto it = rooms.begin(); it != rooms.end(); ++it)
		{
			//remove the user from all the possible rooms
			//this is a bit of an overkill, but it's the safest way to ensure that no room will have the user we want to get rid of in it
			(*it)->removeUser(this->m_user);
		}
		this->m_factory->getLoginManager()->logout(this->m_user->getUsername());
	}
	catch (...)
	{
		std::cout << "Something went wrong with the signout() method" << std::endl;
		status = STATUS_UNKNOWN_ERROR;
	}

	res.status = status;

	return new RequestResult(serializer.serializeResponse(res), newHandler);
}