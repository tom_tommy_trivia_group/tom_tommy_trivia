#include "RoomMemberRequestHandler.h"

//constructor
RoomMemberRequestHandler::RoomMemberRequestHandler(std::shared_ptr<Room> room, std::shared_ptr<LoggedUser> user, std::shared_ptr<RoomManager> roomManager, RequestHandlerFactory* factory) : m_room(room), m_user(user), m_roomManager(roomManager), m_factory(factory)
{
}

//destructor
RoomMemberRequestHandler::~RoomMemberRequestHandler()
{
	this->m_room.reset();
	this->m_user.reset();
	this->m_roomManager.reset();
}

bool RoomMemberRequestHandler::isRequestRelevant(const Request &req) const
{
	bool isRelevant = false;

	if (req.requestId >= GET_ROOM_STATE && req.requestId <= SIGNOUT)
	{
		isRelevant = true;
	}

	return isRelevant;
}

RequestResult* RoomMemberRequestHandler::handleRequest(Request req)
{
	RequestResult* res = nullptr;

	switch (req.requestId)
	{
	case LEAVE_ROOM:
		res = this->leaveRoom();//return the rooms and the new state
		break;

	case GET_ROOM_STATE:
		res = this->getRoomState();//return the highscores and the new state
		break;

	case SIGNOUT:
		res = this->signout();//signout the user. no need for a result because the user is no longer connected
		break;

	default:
		throw Problem(UNVALID_MSG, "RoomMemberRequestHandler - the request Id is not in one of the current options");
		break;
	}

	return res;
}

//removes the user from the room
RequestResult* RoomMemberRequestHandler::leaveRoom()
{
	JsonResponsePacketSerializer serializer;
	IRequestHandler* newHandler = nullptr;
	LeaveRoomResponse res;
	try
	{
		this->m_room->removeUser(this->m_user);
	}
	catch (...)
	{
		std::cout << "Something went wrong with leaveRoom method" << std::endl;
		res = STATUS_UNKNOWN_ERROR;
	}

	//since the user exited the room, he went back into the menu stage
	newHandler = (IRequestHandler*)(this->m_factory->createMenuRequestHandler(this->m_user));

	return new RequestResult(serializer.serializeResponse(res), newHandler);
}

//returns the state of the room
RequestResult* RoomMemberRequestHandler::getRoomState()
{
	unsigned int status = STATUS_OK;
	JsonResponsePacketSerializer serializer;
	IRequestHandler* newHandler = nullptr;
	GetRoomStateResponse res;
	auto users = this->m_room->getAllUsers();
	try
	{
		if (!this->m_room->getData().isAdminInRoom)
		{//if the admin had left the room it cannot be started anymore
			status = GET_ROOM_STATE_ROOM_WAS_CLOSED;
		}

		res.hasGameBegun = this->m_room->getData().isActive;
		res.isAdminInRoom = this->m_room->getData().isAdminInRoom;

		for (auto it = users.begin(); it != users.end(); ++it)
		{//insert all usernames into the list
			res.players.push_back((*it)->getUsername());
		}

		res.questionCount = this->m_room->getData().m_questionCount;
		res.questionTimeout = this->m_room->getData().m_timePerQustion;
	}
	catch (...)
	{
		std::cout << "Something went wrong with the getRoomState() method" << std::endl;
		status = STATUS_UNKNOWN_ERROR;
	}

	res.status = status;

	if (status == GET_ROOM_STATE_ROOM_WAS_CLOSED)
	{
		//since the admin exited the room, all other members must exit as well
		newHandler = (IRequestHandler*)(this->m_factory->createMenuRequestHandler(this->m_user));

	}

	return new RequestResult(serializer.serializeResponse(res), newHandler);
}

//try to remove the user from the signed list and returns the result of that removal
RequestResult* RoomMemberRequestHandler::signout()
{
	unsigned int status = STATUS_OK;
	JsonResponsePacketSerializer serializer;
	IRequestHandler* newHandler = nullptr;
	LogoutResponse res;
	auto rooms = this->m_roomManager->getRooms();

	try
	{
		//remove the user from this room
		this->m_room->removeUser(this->m_user);

		this->m_factory->getLoginManager()->logout(this->m_user->getUsername());

	}
	catch (...)
	{
		std::cout << "Something went wrong with the signout() method" << std::endl;
		status = STATUS_UNKNOWN_ERROR;
	}

	res.status = status;

	return new RequestResult(serializer.serializeResponse(res), newHandler);

}