#pragma once
#include <iostream>
#include "..\Buffer.h"

class IRequestHandler; //RequestResult can now have it but it doesn't haeve to know what it is

struct RequestResult
{
public:
	std::string result;
	IRequestHandler* newHandler;

	RequestResult(std::string value, IRequestHandler* newHandler) :result(value), newHandler(newHandler){};
	RequestResult(const RequestResult& other);
};