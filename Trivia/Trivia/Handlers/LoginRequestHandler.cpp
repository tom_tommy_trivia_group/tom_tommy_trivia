#include "LoginRequestHandler.h"


//constructor
LoginRequestHandler::LoginRequestHandler(std::shared_ptr<LoginManager> manager, RequestHandlerFactory *factory) : m_loginManager(manager), m_handlerFactory(factory)
{
}

//destructor
LoginRequestHandler::~LoginRequestHandler()
{
//	this->m_handlerFactory.reset();
	this->m_loginManager.reset();
}

//the function checks if the request id matches the handler's type id
bool LoginRequestHandler::isRequestRelevant(const Request &req) const
{
	bool isRelevant = false;

	if ((req.requestId >= MIN_REQ_ID && req.requestId <= MAX_LOGIN_REQ_HANDLER_ID) || req.requestId == SIGNOUT)
	{
		isRelevant = true;
	}

	return isRelevant;
}

//the function handles the request - if it's login it logs the user and if it's signup it signs the user up
RequestResult* LoginRequestHandler::handleRequest(Request req)
{
	RequestResult* res = nullptr;
	switch (req.requestId)
	{
		case LOGIN:
			res = this->login(req);//return the new state that corresponds with the result of the login
			break;
		case SIGNUP:
			res = this->signup(req);//return the new state that corresponds with the result of the signup
			break;
		case SIGNOUT:
			//when the client failed to signup/login but still closed the program
			//he needs to be removed from the state machine
			res = this->signout();
			break;
		default:
			throw Problem(UNVALID_MSG, "LoginRequestHandler - the requesr Id is not in one of the currect options");
	}
	return res;
}

//the function logs the user into the database and than returns the response that the database gave
RequestResult* LoginRequestHandler::login(Request req)
{
	JsonRequestPacketDeserializer des;
	JsonResponsePacketSerializer serializer;
	IRequestHandler* newHandler = nullptr;
	LoginRequest specificRequest(des.deserializeLoginRequest(req.buffer));//using copy construction
	unsigned int loginStatus;
	
	loginStatus = this->m_loginManager->login(specificRequest.username, specificRequest.password);//login the user

	LoginResponse res(loginStatus);

	if (loginStatus == STATUS_OK)
	{
		//if the login was successful than the user moves on to the naxt state - the menu state, so he needs a new handler
		newHandler = (IRequestHandler*)(this->m_handlerFactory->createMenuRequestHandler(this->m_loginManager->getUser(specificRequest.username)));
		
	}

	std::cout << "LoginResponse:" << serializer.serializeResponse(res) << std::endl;
	return new RequestResult(serializer.serializeResponse(res), newHandler);
}

//the function signs the user to the database and than returns the response that the database gave
RequestResult* LoginRequestHandler::signup(Request req)
{
	JsonRequestPacketDeserializer des;
	JsonResponsePacketSerializer serializer;
	SignupRequest specificRequest(des.deserializeSignupRequest(req.buffer));//using copy construction
	IRequestHandler* newHandler = nullptr;
	unsigned int signupStatus;

	signupStatus = this->m_loginManager->signup(specificRequest.username, specificRequest.password, specificRequest.email);//siging the user

	SignupResponse res(signupStatus);

	if (signupStatus == STATUS_OK)
	{
		//login the user
		this->m_loginManager->login(specificRequest.username, specificRequest.password);//login the user
		//create the new menu handler
		newHandler = (IRequestHandler*)(this->m_handlerFactory->createMenuRequestHandler(this->m_loginManager->getUser(specificRequest.username)));
	}

	std::cout << "signupResponse" << serializer.serializeResponse(res) << std::endl;
	return new RequestResult(serializer.serializeResponse(res), newHandler);
}

//the function removes the client (who is not logged because if he were it would be MRH who would handle the logout) from the state machine
RequestResult* LoginRequestHandler::signout()
{
	unsigned int status = STATUS_OK;
	JsonResponsePacketSerializer serializer;
	IRequestHandler* newHandler = nullptr;
	LogoutResponse res;

	res.status = status;

	return new RequestResult(serializer.serializeResponse(res), newHandler);
}
