#pragma once

#include "IrequestHandler.h"
#include "..\JsonRequestPacketDeserializer.h"
#include "..\JsonResponsePacketSerializer.h"
#include "..\RequestHandleFactory.h"

class MenuRequestHandler : public IRequestHandler
{
public:
	MenuRequestHandler(std::shared_ptr<LoggedUser> user, std::shared_ptr<RoomManager> roomManager, std::shared_ptr<HighscoreTable> highscoreTable, RequestHandlerFactory* factory);//constructor
	virtual ~MenuRequestHandler();//destructor

	virtual bool isRequestRelevant(const Request &req) const;//the function checks if the request is relevant
	virtual RequestResult* handleRequest(Request req);//the function does what the user's state requires

private:
	//fields:
	std::shared_ptr<LoggedUser> m_user;
	std::shared_ptr<RoomManager> m_roomManager;
	std::shared_ptr<HighscoreTable> m_highscoreTable;
	RequestHandlerFactory* m_factory;

	//private functions:
	RequestResult* getRooms();//the function gets the existing rooms
	RequestResult* getPlayersInRoom(Request req);//the function gets the players in a given room
	RequestResult* getHighscores();//the function gets the 10 highest scores
	RequestResult* joinRoom(Request req);//the function tries to put the user into a room and returns the result
	RequestResult* createRoom(Request req);//the function creates a room with the user as it's admin
	RequestResult* signout();//the function signs out a user
};