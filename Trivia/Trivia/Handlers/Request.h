#pragma once
#include <iostream>
#include "..\Buffer.h"
#include <ctime>
#include <chrono>

struct Request
{
public:
	//constructor
	Request(int id, std::chrono::system_clock::time_point receivalTime, std::string b);
	Request();

	//fields
	int requestId;
	std::chrono::system_clock::time_point receivalTime;
	std::string buffer;
};
