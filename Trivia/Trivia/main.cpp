#pragma comment (lib, "ws2_32.lib")

#include "WSAInitializer.h"
#include "Server.h"

int main(void)
{
	try
	{
		WSAInitializer wsaInit;
		std::shared_ptr<SQLiteDatabase> db(new SQLiteDatabase);
		std::shared_ptr<RequestHandlerFactory> factory(new RequestHandlerFactory(db));
		std::shared_ptr<Communicator> comm(new Communicator(factory));
		Server* server = new Server(comm, db, factory);
		server->serve();
	}
	catch (std::exception& e)
	{
		std::cout << "Error occured: " << e.what() << std::endl;
	}
	

	system("PAUSE");
	return 0;
}


/*#include "LoginRequestHandler.h"

int main()
{
	//create all the object that loginRequestHandler requires, and than them if its handle request function works
	IDatabase db;
	Buffer b ("a new Generic username");
	int i = 1;
	std::chrono::system_clock::time_point now = std::chrono::system_clock::now();
	Request r = { i, now, b };//create a a new general request
	LoginManager lm(db);//create a login manager
	RequestHandleFactory rhf(lm);//creating a factory using the manager
	LoginRequestHandler lrh(lm, rhf);

	RequestResult rr = lrh.handleRequest(r);
	std::cout << (std::string)rr.result << std::endl;//makes sure handle request works (it does)
	std::cout << lrh.isRequestRelevant(r) <<std::endl;//check the function isRequestRelevant (should return true)
	std::cout <<"response: a new Generic username : " <<(std::string)rr.result << std::endl;
	rhf.createLoginRequestHandler();
	system("PAUSE");
	return 0;
}*/