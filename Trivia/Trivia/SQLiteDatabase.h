#pragma once

#include "IDatabase.h"

class sqlite3;

class SQLiteDatabase : IDatabase
{
public:
	SQLiteDatabase();//constructor
	virtual ~SQLiteDatabase();

	// db queries
	bool open() override;
	void close() override;
	void clear() override;

	//user table functions
	virtual bool signup(std::string username, std::string pass, std::string email);//the function signs up the user
	virtual bool isUserSigned(std::string username, std::string pass);//the function checks if the user is signed so it can login

	//function returns the highscoers
	virtual std::map<std::string, int> getHisgscores();
private:
	sqlite3 * m_db;
	bool transact(std::string transaction, int(*callback)(void*, int, char**, char**) = nullptr, void* data = nullptr);// the function gets the query, callback pointer and data structure and commits a transaction
};

//callback for the user table
int userCallback(void* data, int argc, char **argv, char **azColName);
//callback for the highscore table
int highscoreCallback(void* data, int argc, char **argv, char **azColName);