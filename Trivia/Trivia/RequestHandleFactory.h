#pragma once
#include "Managers\LoginManager.h"
#include "Managers\RoomManager.h"
#include "Managers\HighscoreTable.h"

class LoginRequestHandler;
class MenuRequestHandler;
class RoomAdminRequestHandler;
class RoomMemberRequestHandler;

class RequestHandlerFactory
{
private:
	std::shared_ptr<LoginManager> m_loginManager;
	std::shared_ptr<RoomManager> m_roomManager;
	std::shared_ptr<HighscoreTable> m_highscoreTable;
public:
	RequestHandlerFactory(std::shared_ptr<SQLiteDatabase> db);
	~RequestHandlerFactory();
	RequestHandlerFactory& operator= (RequestHandlerFactory& other);
	std::shared_ptr<LoginManager> getLoginManager();
	std::shared_ptr<RoomManager> getRoomManager();
	std::shared_ptr<HighscoreTable> getHighscoreTable();
	LoginRequestHandler* createLoginRequestHandler();
	MenuRequestHandler* createMenuRequestHandler(std::shared_ptr<LoggedUser> user);
	RoomAdminRequestHandler* createRoomAdminRequestHandler(std::shared_ptr<LoggedUser> user, std::shared_ptr<Room> room);
	RoomMemberRequestHandler* createRoomMemberRequestHandler(std::shared_ptr<LoggedUser> user, std::shared_ptr<Room> room);
};