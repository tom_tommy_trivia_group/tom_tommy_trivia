#include "Communicator.h"

#define NUM 30

//constructor
Communicator::Communicator(std::shared_ptr<RequestHandlerFactory> factory) : m_factory(factory)
{
}

//destructor
Communicator::~Communicator()
{	
	for (auto it = this->m_clients.begin(); it != this->m_clients.end(); ++it)
	{
		try
		{
			/* the only use of the destructor should be for freeing
			resources that was allocated in the constructor */
			closesocket(it->first);
			delete(it->second);
		}
		catch (...)
		{
			for (int i = 0; i < NUM; i++)//it makes it easier to identify the problem in case of user spamming
			{
				std::cout << std::endl;
			}
			std::cout << CLOSING_ERROR << std::endl;
		}
	}

	this->m_clients.clear();
	this->m_factory.reset();
}

//function binds into the server port and listens for clients
void Communicator::bindAndListen()
{
	SOCKET cSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	SOCKET client_socket;

	if (INVALID_SOCKET == cSocket)//if the communicator socket is invalid
	{
		throw std::exception(__FUNCTION__ " - socket");
		std::cout << SOCKET_CREATION_ERROR;
	}

	struct sockaddr_in sa = { 0 };//creating the socket's address

	sa.sin_port = htons(SERVER_PORT); // port that server will listen for
	sa.sin_family = AF_INET;   // must be AF_INET
	sa.sin_addr.s_addr = INADDR_ANY;    // when there are few ip's for the machine. We will use always "INADDR_ANY"

	if (SOCKET_ERROR == bind(cSocket, (struct sockaddr*)&sa, sizeof(sa)))//if the scoket can't bind
	{
		throw std::exception(__FUNCTION__ " - bind");//add a catch
		std::cout << BINDING_ERROR;
	}
		

	// Start listening for incoming requests of clients
	if (SOCKET_ERROR == listen(cSocket, SOMAXCONN))//if the socket failes to listen
	{
		throw std::exception(__FUNCTION__ " - listen");//add catch
		std::cout << LISTENING_ERROR;
	}
		
	std::cout << LISTENING_MSG << SERVER_PORT << std::endl;

	while (true)
	{
		// the main thread is only accepting clients and then adding them to the list of handlers
		std::cout << WAITING_MSG << std::endl;
		client_socket = ::accept(cSocket, NULL, NULL);

		if (INVALID_SOCKET == client_socket)//if the client socket is invalid
		{
			throw std::exception(__FUNCTION__);
			std::cout << CLIENT_SOCKET_INIT_ERROR;
		}

		this->startThreadForNewClient(client_socket);//create a thread for the client
			
	}
}


//the function gets the user's requests and acts depending of the type of the request
void Communicator::handleRequests(SOCKET client_socket)
{
	Helper h;
	int type, length;
	std::string name, content;
	IRequestHandler* temp = nullptr;
	RequestResult * reqRes = nullptr;
	Request req;
	std::cout << "client accepted" << std::endl;

	try
	{
		do
		{
			reqRes = NULL;
			temp = nullptr;
			type = charToIntId(h.getMessageTypeCode(client_socket));
			length = h.getDataLengthFromSocket(client_socket);
			content = h.getContentFromSocket(client_socket, length);
			req.requestId = type;
			req.receivalTime = std::chrono::system_clock::now();
			if (type != GET_ROOMS && type != GET_HIGHSCORES && type != SIGNOUT)
			{//if the request requires further data
				req.buffer = content;
			}

			if (INCORRECT_MSG_TYPE != type)
			{//if the msg type is correct
				if (this->m_clients.count(client_socket) == 0)//if the client had connected for the first time
				{
					this->m_clients.insert(std::pair<SOCKET, IRequestHandler*>(client_socket, (this->m_factory->createLoginRequestHandler())));//adding the new client and it's state into the state machine
				}

				reqRes = this->m_clients.at(client_socket)->handleRequest(req);//letting the handler to handle the request

				if (reqRes->newHandler != nullptr)
				{//if the client matches and the new state is not the same as the current one
					temp = this->m_clients[client_socket];
					this->m_clients[client_socket] = reqRes->newHandler;
					delete temp;
				}

				h.sendData(client_socket, reqRes->result);
			}

			else
			{
				std::exception e("Type is not allowed");
				throw e;
				break;
			}

		} while (type != SIGNOUT);//untill the user signs out
	}
	catch (const Problem& p)
	{
		std::cout << p.what() << std::endl;
	}
	catch (const std::exception& e)
	{
		std::cout << e.what() << std::endl;
		this->m_clients.erase(client_socket);
	}

	temp = this->m_clients[client_socket];
	this->m_clients[client_socket] = nullptr;
	delete temp;

	this->m_clients.erase(client_socket);//delete the client connection
}

//the function creates a new thread for a new client
void Communicator::startThreadForNewClient(SOCKET client_socket)
{
	std::thread t(&Communicator::handleRequests, this, client_socket);
	t.detach();
}

//the function returns the enum counterpart of the char that represent the request id
int charToIntId(char c)
{
	int res;

	switch (c)
	{
	case 'S':
		//Signup
		res = SIGNUP;
		break;

		//Login
	case 'L':
		res = LOGIN;
		break;
	
		//get Rooms
	case 'R':
		res = GET_ROOMS;
		break;

		//get Players in room
	case 'P':
		res = GET_PLAYERS_IN_ROOM;
		break;

		//get Highscores
	case 'H':
		res = GET_HIGHSCORES;
		break;

		//Join room
	case 'J':
		res = JOIN_ROOM;
		break;

		//Create room
	case 'C':
		res = CREATE_ROOM;
		break;

	case 'O':
		//clOse room
		res = CLOSE_ROOM;
		break;

	case 'G':
		//start Game
		res = START_GAME;
		break;

	case 'T':
		//get room sTate
		res = GET_ROOM_STATE;
		break;

	case 'E':
		//lEave roon
		res = LEAVE_ROOM;
		break;

	case 'X':
		res = SIGNOUT;
		break;

	default:
		res = INCORRECT_MSG_TYPE;
		break;
	}//as you can see i ran out of letters around the room stage :(

	return res;
}