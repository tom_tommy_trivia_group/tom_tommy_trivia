#pragma once
#include <map>
#include <thread>
#include "Helper.h"
#include <Windows.h>
#include "serverInfo.h"
#include "printingStuff.h"
#include "Handlers\LoginRequestHandler.h"
#include "Handlers\MenuRequestHandler.h"
#include <iterator>


class Communicator
{
private:
	std::map<SOCKET, IRequestHandler*> m_clients;//a map of the users and their states (changed from a referance to a pointer)
	std::shared_ptr<RequestHandlerFactory> m_factory;//the factory that the communicator uses in order to create the states
	void startThreadForNewClient(SOCKET client_Socket);//function creates threads for new clients (login handlers)
public:
	Communicator(std::shared_ptr<RequestHandlerFactory> factory);//constructor
	~Communicator();//destructor
	void bindAndListen();//function binds into the server port and listens for clients
	void handleRequests(SOCKET client_Socket);//function handles the requests
};

int charToIntId(char c);//the function turnes the char id protocol into int id protocol