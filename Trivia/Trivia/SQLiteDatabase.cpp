#include "SQLiteDatabase.h"
#include <io.h>
#include "sqlite3.h"

//definitions for sqlite tranactions
#define BEGIN "BEGIN;"
#define COMMIT "COMMIT;"
#define ROLLBACK "ROLLBACK;"

//constructor
SQLiteDatabase::SQLiteDatabase()
{
	this->open();
}

//destructor:
SQLiteDatabase::~SQLiteDatabase()
{
	if (this->m_db != nullptr)//if the database is still open
	{
		sqlite3_close(this->m_db);
		this->m_db = nullptr;
	}
}

//db queries:
//opens the database, if there is no dabase (the name is predetermined) it opens a new one
bool SQLiteDatabase::open()
{
	std::string DBName = "TriviaDB.sqlite";
	bool didOpenSucceed = true;
	char* errMsg = nullptr;

	int doesFileExist = _access(DBName.c_str(), 0);
	//
	int result = sqlite3_open(DBName.c_str(), &this->m_db);
	try
	{
		if (result != SQLITE_OK)//if the opening wasn't successful
		{
			this->m_db = nullptr;
			throw std::string("Failed to open DB");
		}

		if (doesFileExist != 0)//need to initiate the file
		{
			//initiate database tables
			const char* initiateForeignKeysQuery = "PRAGMA foreign_keys = ON;";
			const char* initiateUsersTableQuery = "CREATE TABLE IF NOT EXISTS USERS (USERNAME TEXT PRIMARY KEY, PASSWORD TEXT NOT NULL, EMAIL TEXT NOT NULL UNIQUE);";

			const char* initiateHighscoresTableQuery = "CREATE TABLE IF NOT EXISTS HIGHSCORES (USERNAME TEXT PRIMARY KEY, SCORE INT NOT NULL);";

			result = sqlite3_exec(this->m_db, initiateForeignKeysQuery, nullptr, nullptr, &errMsg);//make sure that the foreign keys work

			result = sqlite3_exec(this->m_db, initiateUsersTableQuery, nullptr, nullptr, &errMsg);//create users table
			if (result != SQLITE_OK)
			{
				throw std::string("ERROR:Could not create users table");
			}

			result = sqlite3_exec(this->m_db, initiateHighscoresTableQuery, nullptr, nullptr, &errMsg);//create users table
			if (result != SQLITE_OK)
			{
				throw std::string("ERROR:Could not create highscore table");
			}
		}
	}
	catch (std::string errMessege)
	{
		std::cout << errMessege << std::endl;
		didOpenSucceed = false;
	}

	return didOpenSucceed;
}

//closes the database (the connection between the db and the class)
void SQLiteDatabase::close()
{
	sqlite3_close(this->m_db);
	this->m_db = nullptr;
}

//clears the database's tables
void SQLiteDatabase::clear()
{
	int res;
	char* errMsg = nullptr;
	const char* truncUsersTableQuery = "DELETE FROM USERS;";

	res = sqlite3_exec(this->m_db, BEGIN, nullptr, nullptr, &errMsg);//start the transaction

	try
	{
		res = sqlite3_exec(this->m_db, truncUsersTableQuery, nullptr, nullptr, &errMsg);//trunc the users table
		if (res != SQLITE_OK)
		{
			throw std::string("ERROR: Could not clear database - could not clear users table");
		}
		res = sqlite3_exec(this->m_db, COMMIT, nullptr, nullptr, &errMsg);//save the transaction
	}
	catch (std::string errMessege)
	{
		std::cout << errMessege << std::endl;
		res = sqlite3_exec(this->m_db, ROLLBACK, nullptr, nullptr, &errMsg);//cancel the transaction
	}
}

//signs up the user into the database and returns whether the process was successful
bool SQLiteDatabase::signup(std::string username, std::string pass, std::string email)
{
	std::string transaction = std::string("INSERT INTO USERS VALUES('" + username + "', '" + pass + "', '" + email + "');");

	return this->transact(transaction);
}

//checks if a user is signed into the system(used for login)
bool SQLiteDatabase::isUserSigned(std::string username, std::string pass)
{
	bool isSigned = false;
	std::string transaction = std::string("SELECT * FROM USERS WHERE USERNAME = '" + username + "'AND PASSWORD = '" + pass + "';");
	std::vector<std::string> userData;
	this->transact(transaction, userCallback, &userData);
	return !userData.empty();
}

//function gets the highscores
std::map<std::string, int> SQLiteDatabase::getHisgscores()
{
	std::map<std::string, int> highscores;
	std::string transaction = std::string("SELECT * FROM HIGHSCORES ORDER BY SCORE DESC;");
	this->transact(transaction, highscoreCallback, &highscores);
	return highscores;
}

/*
the function executes a given transaction. if the transaction fails it rolls it back
it is used when data is extracted from the file
*/
bool SQLiteDatabase::transact(std::string transaction, int(*callback)(void*, int, char**, char**), void* data)
{
	int res;
	bool didSucceed = true;
	char * errMsg = nullptr;

	res = sqlite3_exec(this->m_db, BEGIN, nullptr, nullptr, &errMsg);//start the transaction
	try
	{
		res = sqlite3_exec(this->m_db, transaction.c_str(), callback, data, &errMsg);//do a query
		if (res != SQLITE_OK)
		{
			throw(errMsg);

		}
		std::cout << transaction << std::endl;
		res = sqlite3_exec(this->m_db, COMMIT, nullptr, nullptr, &errMsg);//save the transaction
	}
	catch (char* errMsg)
	{
		std::cout << errMsg << std::endl << "Canceling change" << std::endl;
		res = sqlite3_exec(this->m_db, ROLLBACK, nullptr, nullptr, &errMsg);//cancel the transaction
		didSucceed = false;
	}

	return didSucceed;
}

//callback function for the users table
int userCallback(void* data, int argc, char **argv, char **azColName)
{
	std::string name, pass, email;

	for (int i = 0; i < argc; i++)
	{
		if (std::string(azColName[i]) == "USERNAME")
		{
			name = std::string(argv[i]);
		}
		if (std::string(azColName[i]) == "PASSWORD")
		{
			pass = std::string(argv[i]);
		}
		if (std::string(azColName[i]) == "EMAIL")
		{
			email = std::string(argv[i]);
		}
	}

	((std::vector<std::string>*)data)->push_back(name);
	((std::vector<std::string>*)data)->push_back(pass);
	((std::vector<std::string>*)data)->push_back(email);

	return 0;
}

//callback for the highscore table
int highscoreCallback(void* data, int argc, char **argv, char **azColName)
{
	std::map<std::string, int> highscores;
	std::string name;
	int score;

	for (int i = 0; i < argc; i++)
	{
		if (std::string(azColName[i]) == "USERNAME")
		{
			name = std::string(argv[i]);
		}
		if (std::string(azColName[i]) == "PASSWORD")
		{
			score = atoi(argv[i]);
		}
	}

	((std::map<std::string, int>*)data)->insert(std::pair<std::string, int>(name, score));

	return 0;
}