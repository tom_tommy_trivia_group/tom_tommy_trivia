#include "IDatabase.h"

/*
constructor:
input: none (because the database only uses the files that it has constructed / used before)
*/
IDatabase::IDatabase()
{

}

/*
destructor:
input: none (the destuctor does not need to do anything, but disconnect from the database which is related to the type of the db
*/
IDatabase::~IDatabase()
{

}