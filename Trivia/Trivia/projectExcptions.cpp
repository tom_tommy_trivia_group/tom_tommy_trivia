#include "projectExceptions.h"

/*
construct a new Problm element
*/
Problem::Problem(problemKind kind, std::string description)
{
	this->_kind = kind;
	this->_description = description;
}

/*
desciption of the exception.
input: non
output: the description
*/
const char * Problem::what() const
{
	return this->_description.c_str();
}

/*
return the kind of the exeption;
input:non
output: the kind
*/
problemKind Problem::kind()
{
	return this->_kind;
}
