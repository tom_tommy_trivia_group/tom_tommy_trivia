#pragma once

///requestId - the id section of the request protocol
enum requestId {SIGNUP, LOGIN, GET_ROOMS, GET_PLAYERS_IN_ROOM, GET_HIGHSCORES, JOIN_ROOM, CREATE_ROOM, CLOSE_ROOM, START_GAME, GET_ROOM_STATE, LEAVE_ROOM, SIGNOUT};
//request stage - since there are many requests whose handleing is the same, they are grouped together by their shared stage

#define INCORRECT_MSG_TYPE -1

//the different statuses that will be returned
//the first words in each status are the responses that can return it
enum statusId { STATUS_OK, SIGNUP_USER_ALREADY_IN_DB, LOGIN_USER_ISNT_IN_THE_DB, LOGIN_USER_ALREADY_LOGGED, JOIN_ROOM_ROOM_NOT_FOUND, JOIN_ROOM_ROOM_IS_FULL, CREATE_ROOM_ROOM_NAME_IS_USED, GET_ROOM_STATE_ROOM_WAS_CLOSED, STATUS_UNKNOWN_ERROR};

#define MAX_REQ_ID SIGNOUT //the amount of possible request id. must change when adding new requests
#define MIN_REQ_ID SIGNUP
#define MAX_LOGIN_REQ_HANDLER_ID LOGIN
#define MIN_MENU_REQ_HANDLER_ID GET_ROOMS
#define MAN_MENU_REQ_HANDLER_ID CREATE_ROOM

///SignUp Related
	// result msg
	#define SIGNUP_SUCCESSFUL_RESULT "Signup successful"
	#define SIGNUP_FAILED_RESULT "Signup failed"

///LogIn Related
	//result msg
	#define LOGIN_SUCCESSFUL_RESULT "Login successful"
	#define LOGIN_FAILED_RESULT "Login failed"