#pragma once
#include <iostream>
#include <string>
#include <Vector>
#include <map>

class IDatabase
{
public:
	IDatabase();//constructor
	virtual ~IDatabase();//destructor

	//
	virtual bool open() = 0;//the funtion opens the connection between the object and the database
	virtual void close() = 0;//the funtion closes the connection between the object and the database
	virtual void clear() = 0;// the funtion clears the database of content

	//user table functions;
	virtual bool signup(std::string username, std::string pass, std::string email) = 0;//the function signs up the user
	virtual bool isUserSigned(std::string username, std::string pass) = 0;//the function checks if the user is signed so it can login

	//highscore table functions:
	//gets all the highscores
	virtual std::map<std::string, int> getHisgscores() = 0;
};