#pragma once

#include <WinSock2.h>
#include <vector>
#include <string>
#include "RequestIdProtocols.h"

class Helper
{
public:

	char getMessageTypeCode(SOCKET sc);//the function recieves the message type code from the socket
	static int getDataLengthFromSocket(SOCKET sc);//the function recieves the message length from the socket
	static std::string getContentFromSocket(SOCKET sc, int bytesNum);//the function recieves the content from the socket
	static void sendData(SOCKET sc, std::string message);//function sends the data to the client

private:
	static char* getPartFromSocket(SOCKET sc, int bytesNum);//gets a data segment from the socket
	static char* getPartFromSocket(SOCKET sc, int bytesNum, int flags);//gets a data segment from the socket, but with the flags parameter can be augmented

};


#ifdef _DEBUG // vs add this define in debug mode
#include <stdio.h>
// Q: why do we need traces ?
// A: traces are a nice and easy way to detect bugs without even debugging
// or to understand what happened in case we miss the bug in the first time
#define TRACE(msg, ...) printf(msg "\n", __VA_ARGS__);
// for convenient reasons we did the traces in stdout
// at general we would do this in the error stream like that
// #define TRACE(msg, ...) fprintf(stderr, msg "\n", __VA_ARGS__);

#else // we want nothing to be printed in release version
#define TRACE(msg, ...) printf(msg "\n", __VA_ARGS__);
#define TRACE(msg, ...) // do nothing
#endif